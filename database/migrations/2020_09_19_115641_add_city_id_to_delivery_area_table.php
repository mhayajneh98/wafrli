<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCityIdToDeliveryAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_area', function (Blueprint $table) {
            $table->string('area_name_ar')->nullable();
            $table->integer('city_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_area', function (Blueprint $table) {
            $table->dropColumn('city_id');
            $table->dropColumn('area_name_ar');;
        });
    }
}
