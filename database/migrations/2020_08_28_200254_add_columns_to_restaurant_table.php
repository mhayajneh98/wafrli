<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToRestaurantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restaurants', function (Blueprint $table) {
            $table->string('phone_number')->nullable();
            $table->string('cover_image')->nullable();
            $table->string('tax')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('currency')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurants', function (Blueprint $table) {
            $table->removeColumn('phone_number');
            $table->removeColumn('cover_image');
            $table->removeColumn('tax');
            $table->removeColumn('payment_method');
            $table->removeColumn('currency');
        });
    }
}
