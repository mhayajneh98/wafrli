@extends('admin.layouts.master')
@section("title") Edit City - Dashboard
@endsection
@section('content')
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-circle-right2 mr-2"></i>
                    <span class="font-weight-bold mr-2">Editing</span>
                </h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.updateCity') }}" method="POST" enctype="multipart/form-data">
                        <legend class="font-weight-semibold text-uppercase font-size-sm">
                            <i class="icon-address-book mr-2"></i> City
                        </legend>
                        <input type="hidden" name="id" value="{{ $city->id }}">

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Name En:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control form-control-lg" value="{{$city->name_en}}" name="name_en"
                                       placeholder="Name en" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Name Ar:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control form-control-lg"  value="{{$city->name_ar}}" name="name_ar"
                                       placeholder="Name Ar" required>
                            </div>
                        </div>
                        @csrf
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">
                                UPDATE
                                <i class="icon-database-insert ml-1"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection