@extends('admin.layouts.master')
@section("title") Edit Sizes - Dashboard
@endsection
@section('content')
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-circle-right2 mr-2"></i>
                    <span class="font-weight-bold mr-2">Editing</span>
                    <span class="badge badge-primary badge-pill animated flipInX">"{{ $size->name }}"</span>
                </h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.updateSize') }}" method="POST" enctype="multipart/form-data">
                        <legend class="font-weight-semibold text-uppercase font-size-sm">
                            <i class="icon-address-book mr-2"></i> Size Details
                        </legend>
                        <input type="hidden" name="id" value="{{ $size->id }}">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Size Name:</label>
                            <div class="col-lg-9">
                                <input value="{{ $size->name }}" type="text" class="form-control form-control-lg" name="name"
                                       placeholder="Size Name" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Price:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control form-control-lg" value="{{$size->price}}" name="price"
                                       placeholder="Price" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Sizes's Addons:</label>
                            <div class="col-lg-9">
                                @foreach($size->addon_categories as $addonCategory)
                                    <span class="badge badge-flat border-grey-800"
                                          style="font-size: 0.9rem;">{{ $addonCategory->name }}
                            </span>
                                @endforeach

                                @if(count($size->addon_categories))
                                    <button class="btn btn-sm btn-danger badge badge-danger float-right" data-toggle="modal"
                                            data-target="#removeAllAddonConfirmation" type="button" id="removeAllAddons"
                                            style="font-size: 0.9rem;">Remove All Addons
                                    </button>
                                    <input type="hidden" name="remove_all_addons" value="0">

                                    <div id="removeAllAddonConfirmation" class="modal fade mt-5" tabindex="-1">
                                        <div class="modal-dialog modal-xs">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title"><span class="font-weight-bold">Are you sure?</span>
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>
                                                <div class="modal-body">
                                            <span class="help-text">This action will remove all addons from this
                                                item.</span>
                                                    <div class="modal-footer mt-4">
                                                        <button type="submit" class="btn btn-primary">Yes</button>
                                                        <button type="button" class="btn btn-danger"
                                                                data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        $('#removeAllAddons').click(function(event) {
                                            $("[name='remove_all_addons']").val(1);
                                        });
                                        $('#removeAllAddonConfirmation').on('hidden.bs.modal', function () {
                                            $("[name='remove_all_addons']").val(0);
                                        });
                                    </script>
                                @endif
                                <select multiple="multiple" class="form-control select" data-fouc
                                        name="addon_category_item[]">
                                    @foreach($addonCategories as $addonCategory)
                                        <option value="{{ $addonCategory->id }}" class="text-capitalize">
                                            {{ $addonCategory->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Item:</label>
                            <div class="col-lg-9">
                                <select name="items_id" class="form-control form-control-lg">
                                    @foreach($items as $item)
                                        <option @if($item->id == $size->item_id) {{"selected"}} @endif value="{{ $item->id }}" class="text-capitalize">
                                            {{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @csrf
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">
                                UPDATE
                                <i class="icon-database-insert ml-1"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function () {

            $('.select').select2();

        });
    </script>
@endsection