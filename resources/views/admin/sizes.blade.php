@extends('admin.layouts.master')
@section("title") Sizes - Dashboard
@endsection
@section('content')
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-circle-right2 mr-2"></i>
                    @if(empty($query))
                        <span class="font-weight-bold mr-2">TOTAL</span>
                        <span class="badge badge-primary badge-pill animated flipInX">{{ $count }}</span>
                    @else
                        <span class="font-weight-bold mr-2">TOTAL</span>
                        <span class="badge badge-primary badge-pill animated flipInX mr-2">{{ $count }}</span>
                        <span class="font-weight-bold mr-2">Results for "{{ $query }}"</span>
                    @endif
                </h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            <div class="header-elements d-none py-0 mb-3 mb-md-0">
                <div class="breadcrumb">
                    <button type="button" class="btn btn-secondary btn-labeled btn-labeled-left mr-2" id="addNewAddonCategory"
                            data-toggle="modal" data-target="#addNewAddonCategoryModal">
                        <b><i class="icon-plus2"></i></b>
                        Add Sizes
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <form action="{{ route('admin.post.searchSizes') }}" method="GET">
            <div class="form-group form-group-feedback form-group-feedback-right search-box">
                <input type="text" class="form-control form-control-lg search-input"
                       placeholder="Search with addon category name" name="query">
                <div class="form-control-feedback form-control-feedback-lg">
                    <i class="icon-search4"></i>
                </div>
            </div>
            @csrf
        </form>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th style="width: 15%">Created At</th>
                            <th class="text-center" style="width: 10%;"><i class="
                                icon-circle-down2"></i></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($sizes as $size)
                            <tr>
                                <td>{{ $size->name }}</td>
                                <td>{{ $size->created_at->diffForHumans() }}</td>
                                <td class="text-center">
                                    <div class="btn-group btn-group-justified">
                                        <a href="{{ route('admin.editSize', $size->id) }}"
                                           class="badge badge-primary badge-icon"> Edit <i
                                                    class="icon-database-edit2 ml-1"></i></a>

                                        <div class="checkbox checkbox-switchery ml-1" style="padding-top: 0.8rem;">
                                            <label>
                                                <input value="true" type="checkbox" class="action-switch"
                                                       @if($item->is_active) checked="checked" @endif data-id="{{ $item->id }}">
                                            </label>
                                        </div>
                                    </div>
                                </td>
                </div>
                </td>
                </tr>
                @endforeach
                </tbody>
                </table>
                <div class="mt-3">
                    {{ $sizes->links() }}
                </div>
            </div>
        </div>
    </div>
    </div>
    <div id="addNewAddonCategoryModal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><span class="font-weight-bold">Add New Size</span></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('admin.saveNewSize') }}" method="POST" enctype="multipart/form-data" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Size Name:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control form-control-lg" name="name"
                                       placeholder="Size Name" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Price:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control form-control-lg" name="price"
                                       placeholder="Size Price" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Addons Category:</label>
                            <div class="col-lg-9">
                                <select multiple="multiple" class="form-control select" data-fouc
                                        name="addon_category_item[]">
                                    @foreach($addonCategories as $addonCategory)
                                        <option value="{{ $addonCategory->id }}" class="text-capitalize">
                                            {{ $addonCategory->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Item:</label>
                            <div class="col-lg-9">
                                <select name="items_id" class="form-control form-control-lg">
                                    @foreach($items as $item)
                                        <option value="{{ $item->id }}" class="text-capitalize">
                                            {{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @csrf
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">
                                SAVE
                                <i class="icon-database-insert ml-1"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {

            $('.select').select2();

        });
    </script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                let reader = new FileReader();
                reader.onload = function (e) {
                    $('.slider-preview-image')
                        .removeClass('hidden')
                        .attr('src', e.target.result)
                        .width(160)
                        .height(117)
                        .css('borderRadius', '0.275rem');
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        function readURL2(input) {
            if (input.files && input.files[0]) {
                let reader = new FileReader();
                reader.onload = function (e) {
                    $('.slider-preview-image2')
                        .removeClass('hidden')
                        .attr('src', e.target.result)
                        .width(160)
                        .height(117)
                        .css('borderRadius', '0.275rem');
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).ready(function() {

            if (Array.prototype.forEach) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery-primary'));
                elems.forEach(function(html) {
                    var switchery = new Switchery(html, { color: '#2196F3' });
                });
            }
            else {
                var elems = document.querySelectorAll('.switchery-primary');
                for (var i = 0; i < elems.length; i++) {
                    var switchery = new Switchery(elems[i], { color: '#2196F3' });
                }
            }

            $('.form-control-uniform').uniform();

            $('#downloadSampleRestaurantCsv').click(function(event) {
                event.preventDefault();
                window.location.href = "/assets/docs/restaurants-sample-csv.csv";
            });

            $('.rating').numeric({allowThouSep:false,  min: 1, max: 5, maxDecimalPlaces: 1 });
            $('.delivery_time').numeric({allowThouSep:false});
            $('.price_range').numeric({allowThouSep:false});
            $('.latitude').numeric({allowThouSep:false});
            $('.longitude').numeric({allowThouSep:false});
            $('.restaurant_charges').numeric({ allowThouSep:false, maxDecimalPlaces: 2, allowMinus: false });
            $('.delivery_charges').numeric({ allowThouSep:false, maxDecimalPlaces: 2, allowMinus: false });
            $('.commission_rate').numeric({ allowThouSep:false, maxDecimalPlaces: 2, max: 100, allowMinus: false });

            $('.delivery_radius').numeric({ allowThouSep:false, maxDecimalPlaces: 2, allowMinus: false });

            $('.base_delivery_charge').numeric({ allowThouSep:false, maxDecimalPlaces: 2, allowMinus: false });
            $('.base_delivery_distance').numeric({ allowThouSep:false, maxDecimalPlaces: 0, allowMinus: false });
            $('.extra_delivery_charge').numeric({ allowThouSep:false, maxDecimalPlaces: 2, allowMinus: false });
            $('.extra_delivery_distance').numeric({ allowThouSep:false, maxDecimalPlaces: 0, allowMinus: false });

            $('.min_order_price').numeric({ allowThouSep:false, maxDecimalPlaces: 2, allowMinus: false });


            $("[name='delivery_charge_type']").change(function(event) {
                if ($(this).val() == "FIXED") {
                    $("[name='base_delivery_charge']").val(null);
                    $("[name='base_delivery_distance']").val(null);
                    $("[name='extra_delivery_charge']").val(null);
                    $("[name='extra_delivery_distance']").val(null);
                    $('#dynamicChargeDiv').addClass('hidden');
                    $('#deliveryCharge').removeClass('hidden')
                } else {
                    $("[name='delivery_charges']").val(null);
                    $('#deliveryCharge').addClass('hidden');
                    $('#dynamicChargeDiv').removeClass('hidden')
                }
            });

            //Switch Action Function
            if (Array.prototype.forEach) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.action-switch'));
                elems.forEach(function(html) {
                    var switchery = new Switchery(html, { color: '#8360c3' });
                });
            }
            else {
                var elems = document.querySelectorAll('.action-switch');
                for (var i = 0; i < elems.length; i++) {
                    var switchery = new Switchery(elems[i], { color: '#8360c3' });
                }
            }

            $('.action-switch').click(function(event) {
                let id = $(this).attr("data-id")
                let url = "{{ url('/admin/size/disable/') }}/"+id;
                window.location.href = url;
            });
        });

    </script>
@endsection