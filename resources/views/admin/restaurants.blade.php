@extends('admin.layouts.master')
@section("title") Stores - Dashboard
@endsection
@section('content')
<style>
    .delivery-div {
        background-color: #fafafa;
        padding: 1rem;
    }

    .location-search-block {
        position: relative;
        top: -26rem;
        z-index: 999;
    }
</style>
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-circle-right2 mr-2"></i>
                @if(empty($query))
                <span class="font-weight-bold mr-2">TOTAL</span>
                <span class="badge badge-primary badge-pill animated flipInX">{{ $count }}</span>
                @else
                <span class="font-weight-bold mr-2">TOTAL</span>
                <span class="badge badge-primary badge-pill animated flipInX mr-2">{{ $count }}</span>
                <span class="font-weight-bold mr-2">Results for "{{ $query }}"</span>
                @endif
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        <div class="header-elements d-none py-0 mb-3 mb-md-0">
            <div class="breadcrumb">
                <a href="{{ route('admin.sortStores') }}"
                    class="btn btn-secondary btn-labeled btn-labeled-left mr-2">
                    <b><i class="icon-sort"></i></b>
                    Sort Stores
                </a>
                @if(!Request::is('admin/stores/pending-acceptance'))
                <a href="{{ route('admin.pendingAcceptance') }}"
                    class="btn btn-secondary btn-labeled btn-labeled-left mr-2">
                    <b><i class="icon-exclamation"></i></b>
                    Pending Approval Stores
                </a>
                @endif
                @if(Request::is('admin/stores'))
                <button type="button" class="btn btn-secondary btn-labeled btn-labeled-left mr-2" id="addNewRestaurant"
                    data-toggle="modal" data-target="#addNewRestaurantModal">
                    <b><i class="icon-plus2"></i></b>
                    Add New Store
                </button>
                <button type="button" class="btn btn-secondary btn-labeled btn-labeled-left" id="addBulkRestaurant"
                    data-toggle="modal" data-target="#addBulkRestaurantModal">
                    <b><i class="icon-database-insert"></i></b>
                    Bulk CSV Upload
                </button>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="content">
    <form action="{{ route('admin.post.searchRestaurants') }}" method="GET">
        <div class="form-group form-group-feedback form-group-feedback-right search-box">
            <input type="text" class="form-control form-control-lg search-input" placeholder="Search with store name"
                name="query">
            <div class="form-control-feedback form-control-feedback-lg">
                <i class="icon-search4"></i>
            </div>
        </div>
        <button type="submit" class="hidden">Search</button>
        @csrf
    </form>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Owner</th>
                            <th style="width: 15%">Created At</th>
                            <th class="text-center" style="width: 10%;"><i class="
                                icon-circle-down2"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($restaurants as $restaurant)
                        <tr>
                            <td>{{ $restaurant->id }}</td>
                            <td><img src="{{ $restaurant->image }}"
                                    alt="{{ $restaurant->name }}" height="80" width="80"
                                    style="border-radius: 0.275rem;"></td>
                            <td>{{ $restaurant->name }}</td>

                            @if(count($restaurant->users))
                            @php
                                $resUsercount = 0
                            @endphp
                                @foreach($restaurant->users as $restaurantUser)
                                    @if($restaurantUser->hasRole("Store Owner"))
                                    @php
                                        $resUsercount++;
                                    @endphp
                                    <td>
                                        <a href="{{ route('admin.get.editUser', $restaurantUser->id) }}">{{ $restaurantUser->name }}</a>
                                        <a href="{{ route('admin.impersonate', $restaurantUser->id) }}"
                                                    class="badge badge-default badge-icon ml-2"  data-popup="tooltip"
                                        data-placement="left" title="Login as {{ $restaurantUser->name }}" style="border: 1px solid #BDBDBD;"> <i
                                                    class="icon-arrow-right15"></i></a>
                                    </td>
                                    @endif
                                @endforeach
                            @if($resUsercount == 0)
                                <td>
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                        UNASSIGNED
                                    </span>
                                </td>
                            @endif
                            @else
                            <td>
                                <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                    UNASSIGNED
                                </span>
                            </td>
                            @endif

                            <td>{{ $restaurant->created_at->diffForHumans() }}</td>
                            <td class="text-center">
                                <div class="btn-group btn-group-justified align-items-center">
                                    <a href="{{ route('admin.get.editRestaurant', $restaurant->id) }}"
                                        class="badge badge-primary badge-icon"> Edit <i
                                            class="icon-database-edit2 ml-1"></i></a>
                                     <a href="{{ route('admin.getRestaurantItems', $restaurant->id) }}"
                                        class="badge badge-primary badge-icon ml-1" data-popup="tooltip"
                                        title="View {{ $restaurant->name }}'s Items" data-placement="bottom"> <i
                                            class="icon-arrow-right5"></i> </a>

                                    <div class="checkbox checkbox-switchery ml-1" style="padding-top: 0.8rem;">
                                        <label>
                                        <input value="true" type="checkbox" class="action-switch"
                                        @if($restaurant->is_active) checked="checked" @endif data-id="{{ $restaurant->id }}">
                                        </label>
                                    </div>

                                    @if(Request::is('admin/stores/pending-acceptance'))
                                    <a href="{{ route('admin.acceptRestaurant', $restaurant->id) }}"
                                        class="badge badge-primary badge-icon ml-1" data-popup="tooltip" title="Accept"
                                        data-placement="bottom" style="background-color: #FF5722"> <i
                                            class="icon-check"></i> </a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="mt-3">
                    {{ $restaurants->appends($_GET)->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<div id="addNewRestaurantModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="font-weight-bold">Add New Store</span></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('admin.saveNewRestaurant') }}" method="POST" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Store Name:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg" name="name" placeholder="Store Name"
                                required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Description:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg" name="description"
                                placeholder="Store Short Description" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Cover Image:</label>
                        <div class="col-lg-9">
                            <img class="slider-preview-image hidden" />
                            <div class="uploader">
                                <input type="file" class="form-control-lg form-control-uniform" name="cover_image" required
                                       accept="image/x-png,image/gif,image/jpeg" onchange="readURL(this);">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Image:</label>
                        <div class="col-lg-9">
                            <img class="slider-preview-image2 hidden" />
                            <div class="uploader">
                                <input type="file" class="form-control-lg form-control-uniform" name="image" required
                                    accept="image/x-png,image/gif,image/jpeg" onchange="readURL2(this);">
                                <span class="help-text text-muted">Image dimension 160x117</span>
                            </div>
                        </div>
                    </div>
                    {{--<div class="form-group row">--}}
                        {{--<label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Rating:</label>--}}
                        {{--<div class="col-lg-9">--}}
                            {{--<input type="text" class="form-control form-control-lg rating" name="rating"--}}
                                {{--placeholder="Rating from 1-5" required>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Approx Delivery
                            Time:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg delivery_time" name="delivery_time"
                                placeholder="Time in Minutes" required>
                        </div>
                    </div>
                    {{--<div class="form-group row">--}}
                        {{--<label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Approx Price for--}}
                            {{--Two:</label>--}}
                        {{--<div class="col-lg-9">--}}
                            {{--<input type="text" class="form-control form-control-lg price_range" name="price_range"--}}
                                {{--placeholder="Approx Price for 2 People in {{ config('settings.currencyFormat') }}"--}}
                                {{--required>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <hr>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Full Address:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg" name="address"
                                placeholder="Full Address of Store" required>
                        </div>
                    </div>
                    {{--<div class="form-group row">--}}
                        {{--<label class="col-lg-3 col-form-label" data-popup="tooltip"--}}
                            {{--title="Pincode / Postcode / Zip Code" data-placement="bottom">Pincode:</label>--}}
                        {{--<div class="col-lg-9">--}}
                            {{--<input type="text" class="form-control form-control-lg" name="pincode"--}}
                                {{--placeholder="Pincode / Postcode / Zip Code of Store">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="form-group row">--}}
                        {{--<label class="col-lg-3 col-form-label">Land Mark:</label>--}}
                        {{--<div class="col-lg-9">--}}
                            {{--<input type="text" class="form-control form-control-lg" name="landmark"--}}
                                {{--placeholder="Any Near Landmark">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <fieldset class="gllpLatlonPicker">
                        {{-- <div width="100%" id="map" class="gllpMap" style="position: relative; overflow: hidden;"></div> --}}
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label class="col-form-label">Latitude:</label><input type="text"
                                    class="form-control form-control-lg gllpLatitude latitude" value="40.6976701"
                                    name="latitude" placeholder="Latitude of the Store" required="required">
                            </div>
                            <div class="col-lg-6">
                                <label class="col-form-label">Longitude:</label><input type="text"
                                    class="form-control form-control-lg gllpLongitude longitude" value="-74.2598672"
                                    name="longitude" placeholder="Longitude of the Store" required="required">
                            </div>
                        </div>
                        {{-- <input type="hidden" class="gllpZoom" value="20">
                        <div class="d-flex justify-content-center">
                            <div class="col-lg-6 d-flex location-search-block">       
                                <input type="text" class="form-control form-control-lg gllpSearchField" placeholder="Search for resraurant, city or town...">
                                <button type="button" class="btn btn-primary gllpSearchButton">Search</button>
                            </div>
                        </div> --}}
                        <span class="text-muted">You can use services like: <a
                                href="https://www.mapcoordinates.net/en" target="_blank">https://www.mapcoordinates.net/en</a></span>
                        <br> If you enter an invalid Latitude/Longitude the map system might crash with a white screen.
                    </fieldset>
                    <hr>
                    {{--<div class="form-group row">--}}
                        {{--<label class="col-lg-3 col-form-label">Certificate/License Code:</label>--}}
                        {{--<div class="col-lg-9">--}}
                            {{--<input type="text" class="form-control form-control-lg" name="certificate"--}}
                                {{--placeholder="Certificate Code or License Code">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Store Charge (Packing/Extra):</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg restaurant_charges"
                                name="restaurant_charges"
                                placeholder="Store Charge in {{ config('settings.currencyFormat') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Payment Method:</label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" name="payment_method" required>
                                <option value="1" class="text-capitalize">Cash</option>
                                <option value="2" class="text-capitalize">Online Gateway</option>
                                <option value="3" class="text-capitalize">Both Cash & Online Gateway</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Currency:</label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" name="currency" required>
                                <option value="AED" @if(config('settings.currencyId')=="AED" ) selected
                                        @endif>AED</option>
                                <option value="AFN" @if(config('settings.currencyId')=="AFN" ) selected
                                        @endif>AFN</option>
                                <option value="ALL" @if(config('settings.currencyId')=="ALL" ) selected
                                        @endif>ALL</option>
                                <option value="AMD" @if(config('settings.currencyId')=="AMD" ) selected
                                        @endif>AMD</option>
                                <option value="ANG" @if(config('settings.currencyId')=="ANG" ) selected
                                        @endif>ANG</option>
                                <option value="ANG" @if(config('settings.currencyId')=="ANG" ) selected
                                        @endif>ANG</option>
                                <option value="AOA" @if(config('settings.currencyId')=="AOA" ) selected
                                        @endif>AOA</option>
                                <option value="ARS" @if(config('settings.currencyId')=="ARS" ) selected
                                        @endif>ARS</option>
                                <option value="AUD" @if(config('settings.currencyId')=="AUD" ) selected
                                        @endif>AUD</option>
                                <option value="AWG" @if(config('settings.currencyId')=="AWG" ) selected
                                        @endif>AWG</option>
                                <option value="AZN" @if(config('settings.currencyId')=="AZN" ) selected
                                        @endif>AZN</option>
                                <option value="BAM" @if(config('settings.currencyId')=="BAM" ) selected
                                        @endif>BAM</option>
                                <option value="BBD" @if(config('settings.currencyId')=="BBD" ) selected
                                        @endif>BBD</option>
                                <option value="BDT" @if(config('settings.currencyId')=="BDT" ) selected
                                        @endif>BDT</option>
                                <option value="BGN" @if(config('settings.currencyId')=="BGN" ) selected
                                        @endif>BGN</option>
                                <option value="BHD" @if(config('settings.currencyId')=="BHD" ) selected
                                        @endif>BHD</option>
                                <option value="BIF" @if(config('settings.currencyId')=="BIF" ) selected
                                        @endif>BIF</option>
                                <option value="BMD" @if(config('settings.currencyId')=="BMD" ) selected
                                        @endif>BMD</option>
                                <option value="BND" @if(config('settings.currencyId')=="BND" ) selected
                                        @endif>BND</option>
                                <option value="BOB" @if(config('settings.currencyId')=="BOB" ) selected
                                        @endif>BOB</option>
                                <option value="BOV" @if(config('settings.currencyId')=="BOV" ) selected
                                        @endif>BOV</option>
                                <option value="BRL" @if(config('settings.currencyId')=="BRL" ) selected
                                        @endif>BRL</option>
                                <option value="BSD" @if(config('settings.currencyId')=="BSD" ) selected
                                        @endif>BSD</option>
                                <option value="BTN" @if(config('settings.currencyId')=="BTN" ) selected
                                        @endif>BTN</option>
                                <option value="BWP" @if(config('settings.currencyId')=="BWP" ) selected
                                        @endif>BWP</option>
                                <option value="BYN" @if(config('settings.currencyId')=="BYN" ) selected
                                        @endif>BYN</option>
                                <option value="BZD" @if(config('settings.currencyId')=="BZD" ) selected
                                        @endif>BZD</option>
                                <option value="CAD" @if(config('settings.currencyId')=="CAD" ) selected
                                        @endif>CAD</option>
                                <option value="CDF" @if(config('settings.currencyId')=="CDF" ) selected
                                        @endif>CDF</option>
                                <option value="CHE" @if(config('settings.currencyId')=="CHE" ) selected
                                        @endif>CHE</option>
                                <option value="CHF" @if(config('settings.currencyId')=="CHF" ) selected
                                        @endif>CHF</option>
                                <option value="CHW" @if(config('settings.currencyId')=="CHW" ) selected
                                        @endif>CHW</option>
                                <option value="CLF" @if(config('settings.currencyId')=="CLF" ) selected
                                        @endif>CLF</option>
                                <option value="CLP" @if(config('settings.currencyId')=="CLP" ) selected
                                        @endif>CLP</option>
                                <option value="CNY" @if(config('settings.currencyId')=="CNY" ) selected
                                        @endif>CNY</option>
                                <option value="COP" @if(config('settings.currencyId')=="COP" ) selected
                                        @endif>COP</option>
                                <option value="COU" @if(config('settings.currencyId')=="COU" ) selected
                                        @endif>COU</option>
                                <option value="CRC" @if(config('settings.currencyId')=="CRC" ) selected
                                        @endif>CRC</option>
                                <option value="CUC" @if(config('settings.currencyId')=="CUC" ) selected
                                        @endif>CUC</option>
                                <option value="CVE" @if(config('settings.currencyId')=="CVE" ) selected
                                        @endif>CVE</option>
                                <option value="CZK" @if(config('settings.currencyId')=="CZK" ) selected
                                        @endif>CZK</option>
                                <option value="DJF" @if(config('settings.currencyId')=="DJF" ) selected
                                        @endif>DJF</option>
                                <option value="DKK" @if(config('settings.currencyId')=="DKK" ) selected
                                        @endif>DKK</option>
                                <option value="DOP" @if(config('settings.currencyId')=="DOP" ) selected
                                        @endif>DOP</option>
                                <option value="DZD" @if(config('settings.currencyId')=="DZD" ) selected
                                        @endif>DZD</option>
                                <option value="EGP" @if(config('settings.currencyId')=="EGP" ) selected
                                        @endif>EGP</option>
                                <option value="ERN" @if(config('settings.currencyId')=="ERN" ) selected
                                        @endif>ERN</option>
                                <option value="ETB" @if(config('settings.currencyId')=="ETB" ) selected
                                        @endif>ETB</option>
                                <option value="EUR" @if(config('settings.currencyId')=="EUR" ) selected
                                        @endif>EUR</option>
                                <option value="FJD" @if(config('settings.currencyId')=="FJD" ) selected
                                        @endif>FJD</option>
                                <option value="FKP" @if(config('settings.currencyId')=="FKP" ) selected
                                        @endif>FKP</option>
                                <option value="GBP" @if(config('settings.currencyId')=="GBP" ) selected
                                        @endif>GBP</option>
                                <option value="GEL" @if(config('settings.currencyId')=="GEL" ) selected
                                        @endif>GEL</option>
                                <option value="GHS" @if(config('settings.currencyId')=="GHS" ) selected
                                        @endif>GHS</option>
                                <option value="GIP" @if(config('settings.currencyId')=="GIP" ) selected
                                        @endif>GIP</option>
                                <option value="GMD" @if(config('settings.currencyId')=="GMD" ) selected
                                        @endif>GMD</option>
                                <option value="GNF" @if(config('settings.currencyId')=="GNF" ) selected
                                        @endif>GNF</option>
                                <option value="GTQ" @if(config('settings.currencyId')=="GTQ" ) selected
                                        @endif>GTQ</option>
                                <option value="GYD" @if(config('settings.currencyId')=="GYD" ) selected
                                        @endif>GYD</option>
                                <option value="HKD" @if(config('settings.currencyId')=="HKD" ) selected
                                        @endif>HKD</option>
                                <option value="HNL" @if(config('settings.currencyId')=="HNL" ) selected
                                        @endif>HNL</option>
                                <option value="HRK" @if(config('settings.currencyId')=="HRK" ) selected
                                        @endif>HRK</option>
                                <option value="HTG" @if(config('settings.currencyId')=="HTG" ) selected
                                        @endif>HTG</option>
                                <option value="HUF" @if(config('settings.currencyId')=="HUF" ) selected
                                        @endif>HUF</option>
                                <option value="IDR" @if(config('settings.currencyId')=="IDR" ) selected
                                        @endif>IDR</option>
                                <option value="ILS" @if(config('settings.currencyId')=="ILS" ) selected
                                        @endif>ILS</option>
                                <option value="INR" @if(config('settings.currencyId')=="INR" ) selected
                                        @endif>INR</option>
                                <option value="IQD" @if(config('settings.currencyId')=="IQD" ) selected
                                        @endif>IQD</option>
                                <option value="IRR" @if(config('settings.currencyId')=="IRR" ) selected
                                        @endif>IRR</option>
                                <option value="ISK" @if(config('settings.currencyId')=="ISK" ) selected
                                        @endif>ISK</option>
                                <option value="JMD" @if(config('settings.currencyId')=="JMD" ) selected
                                        @endif>JMD</option>
                                <option value="JOD" @if(config('settings.currencyId')=="JOD" ) selected
                                        @endif>JOD</option>
                                <option value="JPY" @if(config('settings.currencyId')=="JPY" ) selected
                                        @endif>JPY</option>
                                <option value="KES" @if(config('settings.currencyId')=="KES" ) selected
                                        @endif>KES</option>
                                <option value="KGS" @if(config('settings.currencyId')=="KGS" ) selected
                                        @endif>KGS</option>
                                <option value="KHR" @if(config('settings.currencyId')=="KHR" ) selected
                                        @endif>KHR</option>
                                <option value="KMF" @if(config('settings.currencyId')=="KMF" ) selected
                                        @endif>KMF</option>
                                <option value="KPW" @if(config('settings.currencyId')=="KPW" ) selected
                                        @endif>KPW</option>
                                <option value="KRW" @if(config('settings.currencyId')=="KRW" ) selected
                                        @endif>KRW</option>
                                <option value="KWD" @if(config('settings.currencyId')=="KWD" ) selected
                                        @endif>KWD</option>
                                <option value="KYD" @if(config('settings.currencyId')=="KYD" ) selected
                                        @endif>KYD</option>
                                <option value="KZT" @if(config('settings.currencyId')=="KZT" ) selected
                                        @endif>KZT</option>
                                <option value="LAK" @if(config('settings.currencyId')=="LAK" ) selected
                                        @endif>LAK</option>
                                <option value="LBP" @if(config('settings.currencyId')=="LBP" ) selected
                                        @endif>LBP</option>
                                <option value="LKR" @if(config('settings.currencyId')=="LKR" ) selected
                                        @endif>LKR</option>
                                <option value="LRD" @if(config('settings.currencyId')=="LRD" ) selected
                                        @endif>LRD</option>
                                <option value="LSL" @if(config('settings.currencyId')=="LSL" ) selected
                                        @endif>LSL</option>
                                <option value="LYD" @if(config('settings.currencyId')=="LYD" ) selected
                                        @endif>LYD</option>
                                <option value="MAD" @if(config('settings.currencyId')=="MAD" ) selected
                                        @endif>MAD</option>
                                <option value="MDL" @if(config('settings.currencyId')=="MDL" ) selected
                                        @endif>MDL</option>
                                <option value="MGA" @if(config('settings.currencyId')=="MGA" ) selected
                                        @endif>MGA</option>
                                <option value="MKD" @if(config('settings.currencyId')=="MKD" ) selected
                                        @endif>MKD</option>
                                <option value="MMK" @if(config('settings.currencyId')=="MMK" ) selected
                                        @endif>MMK</option>
                                <option value="MNT" @if(config('settings.currencyId')=="MNT" ) selected
                                        @endif>MNT</option>
                                <option value="MOP" @if(config('settings.currencyId')=="MOP" ) selected
                                        @endif>MOP</option>
                                <option value="MRU" @if(config('settings.currencyId')=="MRU" ) selected
                                        @endif>MRU</option>
                                <option value="MUR" @if(config('settings.currencyId')=="MUR" ) selected
                                        @endif>MUR</option>
                                <option value="MVR" @if(config('settings.currencyId')=="MVR" ) selected
                                        @endif>MVR</option>
                                <option value="MWK" @if(config('settings.currencyId')=="MWK" ) selected
                                        @endif>MWK</option>
                                <option value="MXN" @if(config('settings.currencyId')=="MXN" ) selected
                                        @endif>MXN</option>
                                <option value="MXV" @if(config('settings.currencyId')=="MXV" ) selected
                                        @endif>MXV</option>
                                <option value="MYR" @if(config('settings.currencyId')=="MYR" ) selected
                                        @endif>MYR</option>
                                <option value="MZN" @if(config('settings.currencyId')=="MZN" ) selected
                                        @endif>MZN</option>
                                <option value="NAD" @if(config('settings.currencyId')=="NAD" ) selected
                                        @endif>NAD</option>
                                <option value="NGN" @if(config('settings.currencyId')=="NGN" ) selected
                                        @endif>NGN</option>
                                <option value="NIO" @if(config('settings.currencyId')=="NIO" ) selected
                                        @endif>NIO</option>
                                <option value="NOK" @if(config('settings.currencyId')=="NOK" ) selected
                                        @endif>NOK</option>
                                <option value="NPR" @if(config('settings.currencyId')=="NPR" ) selected
                                        @endif>NPR</option>
                                <option value="NZD" @if(config('settings.currencyId')=="NZD" ) selected
                                        @endif>NZD</option>
                                <option value="OMR" @if(config('settings.currencyId')=="OMR" ) selected
                                        @endif>OMR</option>
                                <option value="PAB" @if(config('settings.currencyId')=="PAB" ) selected
                                        @endif>PAB</option>
                                <option value="PEN" @if(config('settings.currencyId')=="PEN" ) selected
                                        @endif>PEN</option>
                                <option value="PGK" @if(config('settings.currencyId')=="PGK" ) selected
                                        @endif>PGK</option>
                                <option value="PHP" @if(config('settings.currencyId')=="PHP" ) selected
                                        @endif>PHP</option>
                                <option value="PKR" @if(config('settings.currencyId')=="PKR" ) selected
                                        @endif>PKR</option>
                                <option value="PLN" @if(config('settings.currencyId')=="PLN" ) selected
                                        @endif>PLN</option>
                                <option value="PYG" @if(config('settings.currencyId')=="PYG" ) selected
                                        @endif>PYG</option>
                                <option value="QAR" @if(config('settings.currencyId')=="QAR" ) selected
                                        @endif>QAR</option>
                                <option value="RON" @if(config('settings.currencyId')=="RON" ) selected
                                        @endif>RON</option>
                                <option value="RSD" @if(config('settings.currencyId')=="RSD" ) selected
                                        @endif>RSD</option>
                                <option value="RUB" @if(config('settings.currencyId')=="RUB" ) selected
                                        @endif>RUB</option>
                                <option value="RWF" @if(config('settings.currencyId')=="RWF" ) selected
                                        @endif>RWF</option>
                                <option value="SAR" @if(config('settings.currencyId')=="SAR" ) selected
                                        @endif>SAR</option>
                                <option value="SBD" @if(config('settings.currencyId')=="SBD" ) selected
                                        @endif>SBD</option>
                                <option value="SCR" @if(config('settings.currencyId')=="SCR" ) selected
                                        @endif>SCR</option>
                                <option value="SDG" @if(config('settings.currencyId')=="SDG" ) selected
                                        @endif>SDG</option>
                                <option value="SEK" @if(config('settings.currencyId')=="SEK" ) selected
                                        @endif>SEK</option>
                                <option value="SGD" @if(config('settings.currencyId')=="SGD" ) selected
                                        @endif>SGD</option>
                                <option value="SHP" @if(config('settings.currencyId')=="SHP" ) selected
                                        @endif>SHP</option>
                                <option value="SLL" @if(config('settings.currencyId')=="SLL" ) selected
                                        @endif>SLL</option>
                                <option value="SOS" @if(config('settings.currencyId')=="SOS" ) selected
                                        @endif>SOS</option>
                                <option value="SRD" @if(config('settings.currencyId')=="SRD" ) selected
                                        @endif>SRD</option>
                                <option value="SSP" @if(config('settings.currencyId')=="SSP" ) selected
                                        @endif>SSP</option>
                                <option value="STN" @if(config('settings.currencyId')=="STN" ) selected
                                        @endif>STN</option>
                                <option value="SVC" @if(config('settings.currencyId')=="SVC" ) selected
                                        @endif>SVC</option>
                                <option value="SYP" @if(config('settings.currencyId')=="SYP" ) selected
                                        @endif>SYP</option>
                                <option value="SZL" @if(config('settings.currencyId')=="SZL" ) selected
                                        @endif>SZL</option>
                                <option value="THB" @if(config('settings.currencyId')=="THB" ) selected
                                        @endif>THB</option>
                                <option value="TJS" @if(config('settings.currencyId')=="TJS" ) selected
                                        @endif>TJS</option>
                                <option value="TMT" @if(config('settings.currencyId')=="TMT" ) selected
                                        @endif>TMT</option>
                                <option value="TND" @if(config('settings.currencyId')=="TND" ) selected
                                        @endif>TND</option>
                                <option value="TOP" @if(config('settings.currencyId')=="TOP" ) selected
                                        @endif>TOP</option>
                                <option value="TRY" @if(config('settings.currencyId')=="TRY" ) selected
                                        @endif>TRY</option>
                                <option value="TTD" @if(config('settings.currencyId')=="TTD" ) selected
                                        @endif>TTD</option>
                                <option value="TWD" @if(config('settings.currencyId')=="TWD" ) selected
                                        @endif>TWD</option>
                                <option value="TZS" @if(config('settings.currencyId')=="TZS" ) selected
                                        @endif>TZS</option>
                                <option value="UAH" @if(config('settings.currencyId')=="UAH" ) selected
                                        @endif>UAH</option>
                                <option value="UGX" @if(config('settings.currencyId')=="UGX" ) selected
                                        @endif>UGX</option>
                                <option value="USD" @if(config('settings.currencyId')=="USD" ) selected
                                        @endif>USD</option>
                                <option value="USN" @if(config('settings.currencyId')=="USN" ) selected
                                        @endif>USN</option>
                                <option value="UYI" @if(config('settings.currencyId')=="UYI" ) selected
                                        @endif>UYI</option>
                                <option value="UYU" @if(config('settings.currencyId')=="UYU" ) selected
                                        @endif>UYU</option>
                                <option value="UZS" @if(config('settings.currencyId')=="UZS" ) selected
                                        @endif>UZS</option>
                                <option value="VEF" @if(config('settings.currencyId')=="VEF" ) selected
                                        @endif>VEF</option>
                                <option value="VND" @if(config('settings.currencyId')=="VND" ) selected
                                        @endif>VND</option>
                                <option value="VUV" @if(config('settings.currencyId')=="VUV" ) selected
                                        @endif>VUV</option>
                                <option value="WST" @if(config('settings.currencyId')=="WST" ) selected
                                        @endif>WST</option>
                                <option value="XAF" @if(config('settings.currencyId')=="XAF" ) selected
                                        @endif>XAF</option>
                                <option value="XCD" @if(config('settings.currencyId')=="XCD" ) selected
                                        @endif>XCD</option>
                                <option value="XDR" @if(config('settings.currencyId')=="XDR" ) selected
                                        @endif>XDR</option>
                                <option value="XOF" @if(config('settings.currencyId')=="XOF" ) selected
                                        @endif>XOF</option>
                                <option value="XPF" @if(config('settings.currencyId')=="XPF" ) selected
                                        @endif>XPF</option>
                                <option value="XSU" @if(config('settings.currencyId')=="XSU" ) selected
                                        @endif>XSU</option>
                                <option value="XUA" @if(config('settings.currencyId')=="XUA" ) selected
                                        @endif>XUA</option>
                                <option value="YER" @if(config('settings.currencyId')=="YER" ) selected
                                        @endif>YER</option>
                                <option value="ZAR" @if(config('settings.currencyId')=="ZAR" ) selected
                                        @endif>ZAR</option>
                                <option value="ZMW" @if(config('settings.currencyId')=="ZMW" ) selected
                                        @endif>ZMW</option>
                                <option value="ZWL" @if(config('settings.currencyId')=="ZWL" ) selected
                                        @endif>ZWL</option>
                            </select>
                        </div>
                    </div>
                    {{--@if(config("settings.enSPU") == "true")--}}
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Delivery Type:</label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" name="delivery_type" required>
                                <option value="1" class="text-capitalize">Delivery</option>
                                <option value="2" class="text-capitalize">Self Pickup</option>
                                <option value="3" class="text-capitalize">Both Delivery & Self Pickup</option>
                            </select>
                        </div>
                    </div>
                    {{--@endif--}}
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Delivery Radius in Km:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg delivery_radius"
                                name="delivery_radius"
                                placeholder="Delivery Radius in KM (If left blank, delivery radius will be set to 10 KM)">
                        </div>
                    </div>
                    <div class="delivery-div">
                        {{--<div class="form-group row">--}}
                            {{--<label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Delivery Charge--}}
                                {{--Type:</label>--}}
                            {{--<div class="col-lg-9">--}}
                                {{--<select class="form-control select-search" name="delivery_charge_type" required>--}}
                                    {{--<option value="FIXED" class="text-capitalize">Fixed Charge</option>--}}
                                    {{--<option value="DYNAMIC" class="text-capitalize">Dynamic Charge</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group row" id="deliveryCharge">--}}
                            {{--<label class="col-lg-3 col-form-label">Delivery Charge:</label>--}}
                            {{--<div class="col-lg-9">--}}
                                {{--<input type="text" class="form-control form-control-lg delivery_charges"--}}
                                    {{--name="delivery_charges"--}}
                                    {{--placeholder="Delivery Charge in {{ config('settings.currencyFormat') }}">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div id="dynamicChargeDiv" class="hidden">
                            <div class="form-group">
                                <div class="col-lg-12 row">
                                    <div class="col-lg-3">
                                        <label class="col-lg-12 col-form-label">Base Delivery Charge:</label>
                                        <input type="text" class="form-control form-control-lg base_delivery_charge"
                                            name="base_delivery_charge"
                                            placeholder="In {{ config('settings.currencyFormat') }}">
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="col-lg-12 col-form-label">Base Delivery Distance:</label>
                                        <input type="text" class="form-control form-control-lg base_delivery_distance"
                                            name="base_delivery_distance" placeholder="In Kilometer (KM)">
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="col-lg-12 col-form-label">Extra Delivery Charge:</label>
                                        <input type="text" class="form-control form-control-lg extra_delivery_charge"
                                            name="extra_delivery_charge"
                                            placeholder="In {{ config('settings.currencyFormat') }}">
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="col-lg-12 col-form-label">Extra Delivery Distance:</label>
                                        <input type="text" class="form-control form-control-lg extra_delivery_distance"
                                            name="extra_delivery_distance" placeholder="In Kilometer (KM)">
                                    </div>
                                </div>
                                <p class="help-text mt-2 mb-0 text-muted"> Base delivery charges will be applied to the
                                    base delivery distance. And for every extra delivery distance, extra delivery charge
                                    will be applied.</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Is Pure Veg?</label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switchery mt-2">
                                <label>
                                    <input value="true" type="checkbox" class="switchery-primary" checked="checked"
                                        name="is_pureveg">
                                </label>
                            </div>
                        </div>
                    </div>
                    {{--<div class="form-group row">--}}
                        {{--<label class="col-lg-3 col-form-label">Is Featured?</label>--}}
                        {{--<div class="col-lg-9">--}}
                            {{--<div class="checkbox checkbox-switchery mt-2">--}}
                                {{--<label>--}}
                                    {{--<input value="true" type="checkbox" class="switchery-primary" name="is_featured">--}}
                                {{--</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Min Order Price <i class="icon-question3 ml-1"
                                data-popup="tooltip" title="Set the value as 0 if not required"
                                data-placement="top"></i></label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg min_order_price"
                                name="min_order_price"
                                placeholder="Min Cart Value before discount and tax {{ config('settings.currencyFormat') }}"
                                value="0" required="required">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Tax
                            :</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg commission_rate"
                                   name="tax" placeholder="Tax" required>
                        </div>
                    </div>
                    {{--<div class="form-group row">--}}
                        {{--<label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Commission Rate--}}
                            {{--%:</label>--}}
                        {{--<div class="col-lg-9">--}}
                            {{--<input type="text" class="form-control form-control-lg commission_rate"--}}
                                {{--name="commission_rate" placeholder="Commission Rate %" required>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    @csrf
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">
                            SAVE
                            <i class="icon-database-insert ml-1"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<div id="addBulkRestaurantModal" class="modal fade mt-5" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="font-weight-bold">CSV Bulk Upload for Stores</span></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('admin.restaurantBulkUpload') }}" method="POST" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">CSV File: </label>
                        <div class="col-lg-10">
                            <div class="uploader">
                                <input type="file" accept=".csv" name="restaurant_csv"
                                    class="form-control-uniform form-control-lg" required>
                            </div>
                        </div>
                    </div>
                    <div class="text-left">
                        <button type="button" class="btn btn-primary" id="downloadSampleRestaurantCsv">
                            Download Sample CSV
                            <i class="icon-file-download ml-1"></i>
                        </button>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">
                            Upload
                            <i class="icon-database-insert ml-1"></i>
                        </button>
                    </div>
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function readURL(input) {
       if (input.files && input.files[0]) {
           let reader = new FileReader();
           reader.onload = function (e) {
               $('.slider-preview-image')
                   .removeClass('hidden')
                   .attr('src', e.target.result)
                   .width(160)
                   .height(117)
                   .css('borderRadius', '0.275rem');
           };
           reader.readAsDataURL(input.files[0]);
       }
    }
    function readURL2(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function (e) {
                $('.slider-preview-image2')
                    .removeClass('hidden')
                    .attr('src', e.target.result)
                    .width(160)
                    .height(117)
                    .css('borderRadius', '0.275rem');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).ready(function() {

        if (Array.prototype.forEach) {
               var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery-primary'));
               elems.forEach(function(html) {
                   var switchery = new Switchery(html, { color: '#2196F3' });
               });
           }
           else {
               var elems = document.querySelectorAll('.switchery-primary');
               for (var i = 0; i < elems.length; i++) {
                   var switchery = new Switchery(elems[i], { color: '#2196F3' });
               }
           }
        
        $('.form-control-uniform').uniform();
        
        $('#downloadSampleRestaurantCsv').click(function(event) {
            event.preventDefault();
            window.location.href = "/assets/docs/restaurants-sample-csv.csv";
        });
         
         $('.rating').numeric({allowThouSep:false,  min: 1, max: 5, maxDecimalPlaces: 1 });
         $('.delivery_time').numeric({allowThouSep:false});
         $('.price_range').numeric({allowThouSep:false});
         $('.latitude').numeric({allowThouSep:false});
         $('.longitude').numeric({allowThouSep:false});
         $('.restaurant_charges').numeric({ allowThouSep:false, maxDecimalPlaces: 2, allowMinus: false });
         $('.delivery_charges').numeric({ allowThouSep:false, maxDecimalPlaces: 2, allowMinus: false });
         $('.commission_rate').numeric({ allowThouSep:false, maxDecimalPlaces: 2, max: 100, allowMinus: false });
        
        $('.delivery_radius').numeric({ allowThouSep:false, maxDecimalPlaces: 2, allowMinus: false });
        
        $('.base_delivery_charge').numeric({ allowThouSep:false, maxDecimalPlaces: 2, allowMinus: false });
        $('.base_delivery_distance').numeric({ allowThouSep:false, maxDecimalPlaces: 0, allowMinus: false });
        $('.extra_delivery_charge').numeric({ allowThouSep:false, maxDecimalPlaces: 2, allowMinus: false });
        $('.extra_delivery_distance').numeric({ allowThouSep:false, maxDecimalPlaces: 0, allowMinus: false });

        $('.min_order_price').numeric({ allowThouSep:false, maxDecimalPlaces: 2, allowMinus: false });
        
    
         $("[name='delivery_charge_type']").change(function(event) {
             if ($(this).val() == "FIXED") {
                 $("[name='base_delivery_charge']").val(null);
                 $("[name='base_delivery_distance']").val(null);
                 $("[name='extra_delivery_charge']").val(null);
                 $("[name='extra_delivery_distance']").val(null);
                 $('#dynamicChargeDiv').addClass('hidden');
                 $('#deliveryCharge').removeClass('hidden')
             } else {
                 $("[name='delivery_charges']").val(null);
                 $('#deliveryCharge').addClass('hidden');
                 $('#dynamicChargeDiv').removeClass('hidden')
             }
         });
        
        //Switch Action Function
        if (Array.prototype.forEach) {
               var elems = Array.prototype.slice.call(document.querySelectorAll('.action-switch'));
               elems.forEach(function(html) {
                   var switchery = new Switchery(html, { color: '#8360c3' });
               });
           }
           else {
               var elems = document.querySelectorAll('.action-switch');
               for (var i = 0; i < elems.length; i++) {
                   var switchery = new Switchery(elems[i], { color: '#8360c3' });
               }
           }

         $('.action-switch').click(function(event) {
            let id = $(this).attr("data-id")
            let url = "{{ url('/admin/store/disable/') }}/"+id;
            window.location.href = url;
         });
    });
    
</script>
@endsection