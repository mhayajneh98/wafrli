@extends('admin.layouts.master')
@section("title") Cities - Dashboard
@endsection
@section('content')
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-circle-right2 mr-2"></i>
                    @if(empty($query))
                        <span class="font-weight-bold mr-2">TOTAL</span>
                        <span class="badge badge-primary badge-pill animated flipInX">{{ $count }}</span>
                    @else
                        <span class="font-weight-bold mr-2">TOTAL</span>
                        <span class="badge badge-primary badge-pill animated flipInX mr-2">{{ $count }}</span>
                        <span class="font-weight-bold mr-2">Results for "{{ $query }}"</span>
                    @endif
                </h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            <div class="header-elements d-none py-0 mb-3 mb-md-0">
                <div class="breadcrumb">
                    <button type="button" class="btn btn-secondary btn-labeled btn-labeled-left mr-2" id="addNewAddonCategory"
                            data-toggle="modal" data-target="#addNewAddonCategoryModal">
                        <b><i class="icon-plus2"></i></b>
                        Add New City
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>name en</th>
                            <th>name ar</th>
                            <th style="width: 15%">Created At</th>
                            <th class="text-center" style="width: 10%;"><i class="
                                icon-circle-down2"></i></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($cities as $city)
                            <tr>
                                <td>{{$city->name_en}}</td>
                                <td>
                                    {{$city->name_ar}}
                                </td>
                                <td>{{$city->name_en}}</td>
                                <td>{{ $city->created_at->diffForHumans() }}</td>
                                <td class="text-center">
                                    <div class="btn-group btn-group-justified">
                                        <a href="{{ route('admin.editCity', $city->id) }}"
                                           class="badge badge-primary badge-icon"> Edit <i
                                                    class="icon-database-edit2 ml-1"></i></a>

                                    </div>
                                </td>
                </div>
                </td>
                </tr>
                @endforeach
                </tbody>
                </table>
                <div class="mt-3">
                    {{ $cities->links() }}
                </div>
            </div>
        </div>
    </div>
    </div>
    <div id="addNewAddonCategoryModal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><span class="font-weight-bold">Add New City</span></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('admin.saveNewCity') }}" method="POST" enctype="multipart/form-data" enctype="multipart/form-data">

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Name En:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control form-control-lg"  name="name_en"
                                       placeholder="Name en" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Name Ar:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control form-control-lg"  name="name_ar"
                                       placeholder="Name Ar" required>
                            </div>
                        </div>
                        @csrf
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">
                                SAVE
                                <i class="icon-database-insert ml-1"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection