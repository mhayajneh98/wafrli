@extends('admin.layouts.master')
@section("title") Branches - Dashboard
@endsection
@section('content')
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-circle-right2 mr-2"></i>
                    <span class="font-weight-bold mr-2">Editing</span>
                    <span class="badge badge-primary badge-pill animated flipInX">Delivery Areas</span>
                </h4>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('admin.updateDeliveryArea')}}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-8">
                                    <div id="map" class="load_map delivery-zone-map" style="height: 600px;"></div>
                                </div>
                                <div class="col-md-4">
                                    <!--admin_content-->
                                    <div class="admin_content full_row location_marker">
                                        <div class="form_container step_form">
                                            <div class="form_panel" id="delivery-zone">
                                                <div class="box_style form_group_full">
                                                    <h3 class="form_title">
                                                        <?= 'Delivery Areas'?>
                                                        <span onclick="centerMap()" data-toggle="tooltip" data-placement="left" data-original-title="Center map">
                        <i class="fa fa-crosshairs"></i>
                    </span>
                                                    </h3>

                                                    <?php if(count($deliveryarea)) { ?>

                                                    <table class="table table-bordered table-striped">
                                                        <thead>
                                                        <th>City Name</th>
                                                        <th>Zone Name</th>

                                                        <th>Action</th>
                                                        </thead>
                                                        <tbody>

                                                        <?php foreach($deliveryarea as $area) { ?>
                                                        <tr>
                                                            @if($area->city_id != null)
                                                            <td>{{\App\City::find($area->city_id)->name_en}}</td>
                                                            @endif
                                                            <td><?php echo $area->area_name; ?></td>
                                                            <td width="70">

                                                                <a href="javascript:" class="edit_btn" onclick="edit_delivery_area(<?php echo $area->id; ?>);" style="margin:5px">Edit</a>

                                                                <a href="javascript:" class="delete_btn" onclick="delete_delivery_area(<?php echo $area->id; ?>);"  style="margin:5px">Delete</a>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                        </tbody>
                                                    </table>

                                                    <?php }?>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <button type="button" class="btn btn-xs btn-success pull-right" style="display: block;" onclick="reset_data()"><?= 'Add New'?></button>
                                                            <input type="hidden" name="delivery_area_id" id="delivery_area_id" value="">

                                                        </div>
                                                    </div>



                                                    <div id="add_zone" class="zone_form zone_group form_group" data-src="new">

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="input-group form-group field-branch-new_zone_name required">
                                                                    <label for="branch-new_zone_name">Cities</label>
                                                                    <select class="form-control select-search select" id="city_id" name="city_id" required>
                                                                    <option value="0" class="text-capitalize" selected="selected">Choose City</option>
                                                                    @foreach ($cities as $city)
                                                                        <option value="{{ $city->id }}" class="text-capitalize">{{ $city->name_en }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                                </div>
                                                                <div class="input-group form-group field-branch-new_zone_name required">
                                                                    <label for="branch-new_zone_name">Area Name En</label>
                                                                    <input type="text" id="new_zone_name" class="form-control"  name="new_zone_name" aria-required="true" required>

                                                                    <div class="help-block"></div>
                                                                </div>
                                                                <div class="input-group form-group field-branch-new_zone_name required">
                                                                    <label for="branch-new_zone_name">Area Name Ar</label>
                                                                    <input type="text" id="new_zone_name_ar" class="form-control" name="new_zone_name_ar" aria-required="true" required>
                                                                    <div class="help-block"></div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <textarea class="form-control" name="coordinates" id="vendor-coordinates" placeholder="Coordinates" required><?php echo old('coordinates'); ?></textarea>
                                                        <center>
                                                            <ul class="reset" style="list-style: none; margin-top: 15px">
                                                                <li class="actions">

                            <span>
                                <button type="submit" class="btn btn-primary"><?= 'Save'?></button>
                            </span>
                                                                </li>
                                                            </ul>
                                                        </center>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src='http://maps.google.com/maps/api/js?libraries=places,drawing,geometry&key=AIzaSyCpqVEdK8DkCcoLa4rnLIp4tBzde5K6AQs'></script>

    <script>

        function initialize(id = null) {
            // Map Center

            var triangleCoords = [];
                <?php $a = 31.975176952180853; $b = 35.94955492089845;?>

            var myLatLng = new google.maps.LatLng(<?= $a ?>, <?= $b ?>);
            // General Options
            var mapOptions = {
                zoom: 12,
                center: myLatLng,
                mapTypeId: google.maps.MapTypeId.RoadMap
            };
            var map = new google.maps.Map(document.getElementById('map'),mapOptions);

            $('#new_zone_name').val('');
            $('#new_zone_name_ar').val('');
            $('#city_id').val('');

            $('#zone_delivery_fee').val('');
            $('#delivery_area_id').val('');

            if(id == null){

                <?php  $coords = "new google.maps.LatLng(parseFloat(".$a.")-parseFloat(0.001), parseFloat(".$b.")+parseFloat(0.001)), new google.maps.LatLng(parseFloat(".$a.")+parseFloat(0.005), parseFloat(".$b.")+parseFloat(0.005)), new google.maps.LatLng(parseFloat(".$a.")+parseFloat(0.006), parseFloat(".$b.")-parseFloat(0.006)),";
                    ?>

                    triangleCoords = [<?= $coords ?>];

            }else{

                var url = "<?php echo URL::to(''); ?>";
                $.ajax({
                    type: "GET",
                    url: url+"/admin/get_delivery_area",
                    dataType:"json",
                    data: {'id' : id},
                    async: false,
                    success:  function(result){

                        $('#new_zone_name').val(result.area_name);
                        $('#new_zone_name_ar').val(result.area_name_ar);
                        $('#city_id').val(result.city_id);
                        $('#delivery_area_id').val(result.id);

                        document.getElementById('vendor-coordinates').value = result.delivery_area_lat_lon;

                        for (i = 0; i < result.delivery_area.length; ++i) {
                            triangleCoords.push({
                                lat: parseFloat(result.delivery_area[i][0]),
                                lng: parseFloat(result.delivery_area[i][1])
                            });

                        }

                    }
                });
            }

            // Styling & Controls
            myPolygon = new google.maps.Polygon({
                paths: triangleCoords,
                draggable: true, // turn off if it gets annoying
                editable: true,
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.35
            });

            var len = myPolygon.getPath().getLength();
            var latlan = [];
            for (var i = 0; i < len; i++) {
                latlan += "new google.maps.LatLng(" + myPolygon.getPath().getAt(i).toUrlValue(5) + "), ";
            }
            document.getElementById('vendor-coordinates').value = latlan;

            myPolygon.setMap(map);
            google.maps.event.addListener(myPolygon.getPath(), "insert_at", getPolygonCoords);
            google.maps.event.addListener(myPolygon.getPath(), "set_at", getPolygonCoords);

        }

        function getPolygonCoords() {

            var len = myPolygon.getPath().getLength();
            var latlan = [];
            for (var i = 0; i < len; i++) {
                latlan += "new google.maps.LatLng(" + myPolygon.getPath().getAt(i).toUrlValue(5) + "), ";
            }
            document.getElementById('vendor-coordinates').value = latlan;
        }


        $(function(){
            initialize();
        });

        function delete_delivery_area(id)
        {
            var url = "<?php echo URL::to(''); ?>";
            if (confirm("Are you sure you want to delete this"))
            {
                window.location = url+'/admin/delete_delivery_area/' + id;
            }
        }

        function edit_delivery_area(id)
        {
            initialize(id);

        }

        function reset_data()
        {
            initialize();

        }

    </script>
@endsection