@extends('admin.layouts.master')
@section("title") {{__('storeDashboard.spPageTitle')}}
@endsection
@section('content')
<style>
    .delivery-div {
        background-color: #fafafa;
        padding: 1rem;
    }
    .location-search-block {
        position: relative;
        top: -26rem;
        z-index: 999;
    }
</style>
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-circle-right2 mr-2"></i>
                <span class="font-weight-bold mr-2">{{__('storeDashboard.sePageTitleEditing')}}</span>
                <span class="badge badge-primary badge-pill animated flipInX">"{{ $restaurant->name }}"</span>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<div class="content">
    <div class="row">
        <div class="col-md-7">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('restaurant.updateRestaurant') }}" method="POST" enctype="multipart/form-data">
                        <legend class="font-weight-semibold text-uppercase font-size-sm">
                            <i class="icon-store2 mr-2"></i> {{__('storeDashboard.sePageTitleStoreDetails')}}
                        </legend>
                        <input type="hidden" name="id" value="{{ $restaurant->id }}">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>{{__('storeDashboard.seLblStoreName')}}:</label>
                            <div class="col-lg-9">
                                <input value="{{ $restaurant->name }}" type="text" class="form-control form-control-lg" name="name"
                                    placeholder="{{__('storeDashboard.sePhStoreName')}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>{{__('storeDashboard.seLblDescription')}}:</label>
                            <div class="col-lg-9">
                                <input value="{{ $restaurant->description }}" type="text" class="form-control form-control-lg" name="description"
                                    placeholder="{{__('storeDashboard.sePhDescription')}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Cover Image:</label>
                            <div class="col-lg-9">
                                <img src="{{ $restaurant->cover_image }}" alt="Image" width="160" style="border-radius: 0.275rem;">
                                <img class="slider-preview-image hidden" style="border-radius: 0.275rem;"/>
                                <div class="uploader">
                                    <input type="hidden" name="old_cover_image" value="{{ $restaurant->cover_image }}">
                                    <input type="file" class="form-control-uniform" name="cover_image" accept="image/x-png,image/gif,image/jpeg" onchange="readURL(this);">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">{{__('storeDashboard.seLblImage')}}:</label>
                            <div class="col-lg-9">
                                <img src="{{ $restaurant->image }}" alt="Image" width="160" style="border-radius: 0.275rem;">
                                <img class="slider-preview-image2 hidden" style="border-radius: 0.275rem;"/>
                                <div class="uploader">
                                    <input type="hidden" name="old_image" value="{{ $restaurant->image }}">
                                    <input type="file" class="form-control-uniform" name="image" accept="image/x-png,image/gif,image/jpeg" onchange="readURL2(this);">
                                    <span class="help-text text-muted">{{__('storeDashboard.sePhImage')}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>{{__('storeDashboard.seLblApproxDeliveryTime')}}:</label>
                            <div class="col-lg-9">
                                <input value="{{ $restaurant->delivery_time }}" type="text" class="form-control form-control-lg delivery_time" name="delivery_time"
                                    placeholder="{{__('storeDashboard.sePhApproxDeliveryTime')}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Payment Method:</label>
                            <div class="col-lg-9">
                                <select class="form-control select-search" name="payment_method" required>
                                    <option value="1"  @if($restaurant->payment_method == "1") selected="selected" @endif class="text-capitalize">Cash</option>
                                    <option value="2"  @if($restaurant->payment_method == "2") selected="selected" @endif class="text-capitalize">Online Gateway</option>
                                    <option value="3"  @if($restaurant->payment_method == "3") selected="selected" @endif class="text-capitalize">Both Cash & Online Gateway</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Currency:</label>
                            <div class="col-lg-9">
                                <select class="form-control select-search" name="currency" required>
                                    <option value="AED" @if($restaurant->currency=="AED" ) selected
                                            @endif>AED</option>
                                    <option value="AFN" @if($restaurant->currency=="AFN" ) selected
                                            @endif>AFN</option>
                                    <option value="ALL" @if($restaurant->currency=="ALL" ) selected
                                            @endif>ALL</option>
                                    <option value="AMD" @if($restaurant->currency=="AMD" ) selected
                                            @endif>AMD</option>
                                    <option value="ANG" @if($restaurant->currency=="ANG" ) selected
                                            @endif>ANG</option>
                                    <option value="ANG" @if($restaurant->currency=="ANG" ) selected
                                            @endif>ANG</option>
                                    <option value="AOA" @if($restaurant->currency=="AOA" ) selected
                                            @endif>AOA</option>
                                    <option value="ARS" @if($restaurant->currency=="ARS" ) selected
                                            @endif>ARS</option>
                                    <option value="AUD" @if($restaurant->currency=="AUD" ) selected
                                            @endif>AUD</option>
                                    <option value="AWG" @if($restaurant->currency=="AWG" ) selected
                                            @endif>AWG</option>
                                    <option value="AZN" @if($restaurant->currency=="AZN" ) selected
                                            @endif>AZN</option>
                                    <option value="BAM" @if($restaurant->currency=="BAM" ) selected
                                            @endif>BAM</option>
                                    <option value="BBD" @if($restaurant->currency=="BBD" ) selected
                                            @endif>BBD</option>
                                    <option value="BDT" @if($restaurant->currency=="BDT" ) selected
                                            @endif>BDT</option>
                                    <option value="BGN" @if($restaurant->currency=="BGN" ) selected
                                            @endif>BGN</option>
                                    <option value="BHD" @if($restaurant->currency=="BHD" ) selected
                                            @endif>BHD</option>
                                    <option value="BIF" @if($restaurant->currency=="BIF" ) selected
                                            @endif>BIF</option>
                                    <option value="BMD" @if($restaurant->currency=="BMD" ) selected
                                            @endif>BMD</option>
                                    <option value="BND" @if($restaurant->currency=="BND" ) selected
                                            @endif>BND</option>
                                    <option value="BOB" @if($restaurant->currency=="BOB" ) selected
                                            @endif>BOB</option>
                                    <option value="BOV" @if($restaurant->currency=="BOV" ) selected
                                            @endif>BOV</option>
                                    <option value="BRL" @if($restaurant->currency=="BRL" ) selected
                                            @endif>BRL</option>
                                    <option value="BSD" @if($restaurant->currency=="BSD" ) selected
                                            @endif>BSD</option>
                                    <option value="BTN" @if($restaurant->currency=="BTN" ) selected
                                            @endif>BTN</option>
                                    <option value="BWP" @if($restaurant->currency=="BWP" ) selected
                                            @endif>BWP</option>
                                    <option value="BYN" @if($restaurant->currency=="BYN" ) selected
                                            @endif>BYN</option>
                                    <option value="BZD" @if($restaurant->currency=="BZD" ) selected
                                            @endif>BZD</option>
                                    <option value="CAD" @if($restaurant->currency=="CAD" ) selected
                                            @endif>CAD</option>
                                    <option value="CDF" @if($restaurant->currency=="CDF" ) selected
                                            @endif>CDF</option>
                                    <option value="CHE" @if($restaurant->currency=="CHE" ) selected
                                            @endif>CHE</option>
                                    <option value="CHF" @if($restaurant->currency=="CHF" ) selected
                                            @endif>CHF</option>
                                    <option value="CHW" @if($restaurant->currency=="CHW" ) selected
                                            @endif>CHW</option>
                                    <option value="CLF" @if($restaurant->currency=="CLF" ) selected
                                            @endif>CLF</option>
                                    <option value="CLP" @if($restaurant->currency=="CLP" ) selected
                                            @endif>CLP</option>
                                    <option value="CNY" @if($restaurant->currency=="CNY" ) selected
                                            @endif>CNY</option>
                                    <option value="COP" @if($restaurant->currency=="COP" ) selected
                                            @endif>COP</option>
                                    <option value="COU" @if($restaurant->currency=="COU" ) selected
                                            @endif>COU</option>
                                    <option value="CRC" @if($restaurant->currency=="CRC" ) selected
                                            @endif>CRC</option>
                                    <option value="CUC" @if($restaurant->currency=="CUC" ) selected
                                            @endif>CUC</option>
                                    <option value="CVE" @if($restaurant->currency=="CVE" ) selected
                                            @endif>CVE</option>
                                    <option value="CZK" @if($restaurant->currency=="CZK" ) selected
                                            @endif>CZK</option>
                                    <option value="DJF" @if($restaurant->currency=="DJF" ) selected
                                            @endif>DJF</option>
                                    <option value="DKK" @if($restaurant->currency=="DKK" ) selected
                                            @endif>DKK</option>
                                    <option value="DOP" @if($restaurant->currency=="DOP" ) selected
                                            @endif>DOP</option>
                                    <option value="DZD" @if($restaurant->currency=="DZD" ) selected
                                            @endif>DZD</option>
                                    <option value="EGP" @if($restaurant->currency=="EGP" ) selected
                                            @endif>EGP</option>
                                    <option value="ERN" @if($restaurant->currency=="ERN" ) selected
                                            @endif>ERN</option>
                                    <option value="ETB" @if($restaurant->currency=="ETB" ) selected
                                            @endif>ETB</option>
                                    <option value="EUR" @if($restaurant->currency=="EUR" ) selected
                                            @endif>EUR</option>
                                    <option value="FJD" @if($restaurant->currency=="FJD" ) selected
                                            @endif>FJD</option>
                                    <option value="FKP" @if($restaurant->currency=="FKP" ) selected
                                            @endif>FKP</option>
                                    <option value="GBP" @if($restaurant->currency=="GBP" ) selected
                                            @endif>GBP</option>
                                    <option value="GEL" @if($restaurant->currency=="GEL" ) selected
                                            @endif>GEL</option>
                                    <option value="GHS" @if($restaurant->currency=="GHS" ) selected
                                            @endif>GHS</option>
                                    <option value="GIP" @if($restaurant->currency=="GIP" ) selected
                                            @endif>GIP</option>
                                    <option value="GMD" @if($restaurant->currency=="GMD" ) selected
                                            @endif>GMD</option>
                                    <option value="GNF" @if($restaurant->currency=="GNF" ) selected
                                            @endif>GNF</option>
                                    <option value="GTQ" @if($restaurant->currency=="GTQ" ) selected
                                            @endif>GTQ</option>
                                    <option value="GYD" @if($restaurant->currency=="GYD" ) selected
                                            @endif>GYD</option>
                                    <option value="HKD" @if($restaurant->currency=="HKD" ) selected
                                            @endif>HKD</option>
                                    <option value="HNL" @if($restaurant->currency=="HNL" ) selected
                                            @endif>HNL</option>
                                    <option value="HRK" @if($restaurant->currency=="HRK" ) selected
                                            @endif>HRK</option>
                                    <option value="HTG" @if($restaurant->currency=="HTG" ) selected
                                            @endif>HTG</option>
                                    <option value="HUF" @if($restaurant->currency=="HUF" ) selected
                                            @endif>HUF</option>
                                    <option value="IDR" @if($restaurant->currency=="IDR" ) selected
                                            @endif>IDR</option>
                                    <option value="ILS" @if($restaurant->currency=="ILS" ) selected
                                            @endif>ILS</option>
                                    <option value="INR" @if($restaurant->currency=="INR" ) selected
                                            @endif>INR</option>
                                    <option value="IQD" @if($restaurant->currency=="IQD" ) selected
                                            @endif>IQD</option>
                                    <option value="IRR" @if($restaurant->currency=="IRR" ) selected
                                            @endif>IRR</option>
                                    <option value="ISK" @if($restaurant->currency=="ISK" ) selected
                                            @endif>ISK</option>
                                    <option value="JMD" @if($restaurant->currency=="JMD" ) selected
                                            @endif>JMD</option>
                                    <option value="JOD" @if($restaurant->currency=="JOD" ) selected
                                            @endif>JOD</option>
                                    <option value="JPY" @if($restaurant->currency=="JPY" ) selected
                                            @endif>JPY</option>
                                    <option value="KES" @if($restaurant->currency=="KES" ) selected
                                            @endif>KES</option>
                                    <option value="KGS" @if($restaurant->currency=="KGS" ) selected
                                            @endif>KGS</option>
                                    <option value="KHR" @if($restaurant->currency=="KHR" ) selected
                                            @endif>KHR</option>
                                    <option value="KMF" @if($restaurant->currency=="KMF" ) selected
                                            @endif>KMF</option>
                                    <option value="KPW" @if($restaurant->currency=="KPW" ) selected
                                            @endif>KPW</option>
                                    <option value="KRW" @if($restaurant->currency=="KRW" ) selected
                                            @endif>KRW</option>
                                    <option value="KWD" @if($restaurant->currency=="KWD" ) selected
                                            @endif>KWD</option>
                                    <option value="KYD" @if($restaurant->currency=="KYD" ) selected
                                            @endif>KYD</option>
                                    <option value="KZT" @if($restaurant->currency=="KZT" ) selected
                                            @endif>KZT</option>
                                    <option value="LAK" @if($restaurant->currency=="LAK" ) selected
                                            @endif>LAK</option>
                                    <option value="LBP" @if($restaurant->currency=="LBP" ) selected
                                            @endif>LBP</option>
                                    <option value="LKR" @if($restaurant->currency=="LKR" ) selected
                                            @endif>LKR</option>
                                    <option value="LRD" @if($restaurant->currency=="LRD" ) selected
                                            @endif>LRD</option>
                                    <option value="LSL" @if($restaurant->currency=="LSL" ) selected
                                            @endif>LSL</option>
                                    <option value="LYD" @if($restaurant->currency=="LYD" ) selected
                                            @endif>LYD</option>
                                    <option value="MAD" @if($restaurant->currency=="MAD" ) selected
                                            @endif>MAD</option>
                                    <option value="MDL" @if($restaurant->currency=="MDL" ) selected
                                            @endif>MDL</option>
                                    <option value="MGA" @if($restaurant->currency=="MGA" ) selected
                                            @endif>MGA</option>
                                    <option value="MKD" @if($restaurant->currency=="MKD" ) selected
                                            @endif>MKD</option>
                                    <option value="MMK" @if($restaurant->currency=="MMK" ) selected
                                            @endif>MMK</option>
                                    <option value="MNT" @if($restaurant->currency=="MNT" ) selected
                                            @endif>MNT</option>
                                    <option value="MOP" @if($restaurant->currency=="MOP" ) selected
                                            @endif>MOP</option>
                                    <option value="MRU" @if($restaurant->currency=="MRU" ) selected
                                            @endif>MRU</option>
                                    <option value="MUR" @if($restaurant->currency=="MUR" ) selected
                                            @endif>MUR</option>
                                    <option value="MVR" @if($restaurant->currency=="MVR" ) selected
                                            @endif>MVR</option>
                                    <option value="MWK" @if($restaurant->currency=="MWK" ) selected
                                            @endif>MWK</option>
                                    <option value="MXN" @if($restaurant->currency=="MXN" ) selected
                                            @endif>MXN</option>
                                    <option value="MXV" @if($restaurant->currency=="MXV" ) selected
                                            @endif>MXV</option>
                                    <option value="MYR" @if($restaurant->currency=="MYR" ) selected
                                            @endif>MYR</option>
                                    <option value="MZN" @if($restaurant->currency=="MZN" ) selected
                                            @endif>MZN</option>
                                    <option value="NAD" @if($restaurant->currency=="NAD" ) selected
                                            @endif>NAD</option>
                                    <option value="NGN" @if($restaurant->currency=="NGN" ) selected
                                            @endif>NGN</option>
                                    <option value="NIO" @if($restaurant->currency=="NIO" ) selected
                                            @endif>NIO</option>
                                    <option value="NOK" @if($restaurant->currency=="NOK" ) selected
                                            @endif>NOK</option>
                                    <option value="NPR" @if($restaurant->currency=="NPR" ) selected
                                            @endif>NPR</option>
                                    <option value="NZD" @if($restaurant->currency=="NZD" ) selected
                                            @endif>NZD</option>
                                    <option value="OMR" @if($restaurant->currency=="OMR" ) selected
                                            @endif>OMR</option>
                                    <option value="PAB" @if($restaurant->currency=="PAB" ) selected
                                            @endif>PAB</option>
                                    <option value="PEN" @if($restaurant->currency=="PEN" ) selected
                                            @endif>PEN</option>
                                    <option value="PGK" @if($restaurant->currency=="PGK" ) selected
                                            @endif>PGK</option>
                                    <option value="PHP" @if($restaurant->currency=="PHP" ) selected
                                            @endif>PHP</option>
                                    <option value="PKR" @if($restaurant->currency=="PKR" ) selected
                                            @endif>PKR</option>
                                    <option value="PLN" @if($restaurant->currency=="PLN" ) selected
                                            @endif>PLN</option>
                                    <option value="PYG" @if($restaurant->currency=="PYG" ) selected
                                            @endif>PYG</option>
                                    <option value="QAR" @if($restaurant->currency=="QAR" ) selected
                                            @endif>QAR</option>
                                    <option value="RON" @if($restaurant->currency=="RON" ) selected
                                            @endif>RON</option>
                                    <option value="RSD" @if($restaurant->currency=="RSD" ) selected
                                            @endif>RSD</option>
                                    <option value="RUB" @if($restaurant->currency=="RUB" ) selected
                                            @endif>RUB</option>
                                    <option value="RWF" @if($restaurant->currency=="RWF" ) selected
                                            @endif>RWF</option>
                                    <option value="SAR" @if($restaurant->currency=="SAR" ) selected
                                            @endif>SAR</option>
                                    <option value="SBD" @if($restaurant->currency=="SBD" ) selected
                                            @endif>SBD</option>
                                    <option value="SCR" @if($restaurant->currency=="SCR" ) selected
                                            @endif>SCR</option>
                                    <option value="SDG" @if($restaurant->currency=="SDG" ) selected
                                            @endif>SDG</option>
                                    <option value="SEK" @if($restaurant->currency=="SEK" ) selected
                                            @endif>SEK</option>
                                    <option value="SGD" @if($restaurant->currency=="SGD" ) selected
                                            @endif>SGD</option>
                                    <option value="SHP" @if($restaurant->currency=="SHP" ) selected
                                            @endif>SHP</option>
                                    <option value="SLL" @if($restaurant->currency=="SLL" ) selected
                                            @endif>SLL</option>
                                    <option value="SOS" @if($restaurant->currency=="SOS" ) selected
                                            @endif>SOS</option>
                                    <option value="SRD" @if($restaurant->currency=="SRD" ) selected
                                            @endif>SRD</option>
                                    <option value="SSP" @if($restaurant->currency=="SSP" ) selected
                                            @endif>SSP</option>
                                    <option value="STN" @if($restaurant->currency=="STN" ) selected
                                            @endif>STN</option>
                                    <option value="SVC" @if($restaurant->currency=="SVC" ) selected
                                            @endif>SVC</option>
                                    <option value="SYP" @if($restaurant->currency=="SYP" ) selected
                                            @endif>SYP</option>
                                    <option value="SZL" @if($restaurant->currency=="SZL" ) selected
                                            @endif>SZL</option>
                                    <option value="THB" @if($restaurant->currency=="THB" ) selected
                                            @endif>THB</option>
                                    <option value="TJS" @if($restaurant->currency=="TJS" ) selected
                                            @endif>TJS</option>
                                    <option value="TMT" @if($restaurant->currency=="TMT" ) selected
                                            @endif>TMT</option>
                                    <option value="TND" @if($restaurant->currency=="TND" ) selected
                                            @endif>TND</option>
                                    <option value="TOP" @if($restaurant->currency=="TOP" ) selected
                                            @endif>TOP</option>
                                    <option value="TRY" @if($restaurant->currency=="TRY" ) selected
                                            @endif>TRY</option>
                                    <option value="TTD" @if($restaurant->currency=="TTD" ) selected
                                            @endif>TTD</option>
                                    <option value="TWD" @if($restaurant->currency=="TWD" ) selected
                                            @endif>TWD</option>
                                    <option value="TZS" @if($restaurant->currency=="TZS" ) selected
                                            @endif>TZS</option>
                                    <option value="UAH" @if($restaurant->currency=="UAH" ) selected
                                            @endif>UAH</option>
                                    <option value="UGX" @if($restaurant->currency=="UGX" ) selected
                                            @endif>UGX</option>
                                    <option value="USD" @if($restaurant->currency=="USD" ) selected
                                            @endif>USD</option>
                                    <option value="USN" @if($restaurant->currency=="USN" ) selected
                                            @endif>USN</option>
                                    <option value="UYI" @if($restaurant->currency=="UYI" ) selected
                                            @endif>UYI</option>
                                    <option value="UYU" @if($restaurant->currency=="UYU" ) selected
                                            @endif>UYU</option>
                                    <option value="UZS" @if($restaurant->currency=="UZS" ) selected
                                            @endif>UZS</option>
                                    <option value="VEF" @if($restaurant->currency=="VEF" ) selected
                                            @endif>VEF</option>
                                    <option value="VND" @if($restaurant->currency=="VND" ) selected
                                            @endif>VND</option>
                                    <option value="VUV" @if($restaurant->currency=="VUV" ) selected
                                            @endif>VUV</option>
                                    <option value="WST" @if($restaurant->currency=="WST" ) selected
                                            @endif>WST</option>
                                    <option value="XAF" @if($restaurant->currency=="XAF" ) selected
                                            @endif>XAF</option>
                                    <option value="XCD" @if($restaurant->currency=="XCD" ) selected
                                            @endif>XCD</option>
                                    <option value="XDR" @if($restaurant->currency=="XDR" ) selected
                                            @endif>XDR</option>
                                    <option value="XOF" @if($restaurant->currency=="XOF" ) selected
                                            @endif>XOF</option>
                                    <option value="XPF" @if($restaurant->currency=="XPF" ) selected
                                            @endif>XPF</option>
                                    <option value="XSU" @if($restaurant->currency=="XSU" ) selected
                                            @endif>XSU</option>
                                    <option value="XUA" @if($restaurant->currency=="XUA" ) selected
                                            @endif>XUA</option>
                                    <option value="YER" @if($restaurant->currency=="YER" ) selected
                                            @endif>YER</option>
                                    <option value="ZAR" @if($restaurant->currency=="ZAR" ) selected
                                            @endif>ZAR</option>
                                    <option value="ZMW" @if($restaurant->currency=="ZMW" ) selected
                                            @endif>ZMW</option>
                                    <option value="ZWL" @if($restaurant->currency=="ZWL" ) selected
                                            @endif>ZWL</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Delivery Type:</label>
                            <div class="col-lg-9">
                                <select class="form-control select-search" name="delivery_type" required>
                                    <option value="1" class="text-capitalize" @if($restaurant->delivery_type == "1") selected="selected" @endif>Delivery</option>
                                    <option value="2" class="text-capitalize" @if($restaurant->delivery_type == "2") selected="selected" @endif>Self Pickup</option>
                                    <option value="3" class="text-capitalize" @if($restaurant->delivery_type == "3") selected="selected" @endif>Both Delivery & Self Pickup</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Tax
                                :</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control form-control-lg commission_rate"
                                       name="tax" value="{{$restaurant->tax}}" placeholder="Tax" required>
                            </div>
                        </div>
                        {{--<div class="form-group row">--}}
                            {{--<label class="col-lg-3 col-form-label"><span class="text-danger">*</span>{{__('storeDashboard.seLblApproxPriceForTwo')}}:</label>--}}
                            {{--<div class="col-lg-9">--}}
                                {{--<input value="{{ $restaurant->price_range }}" type="text" class="form-control form-control-lg price_range" name="price_range"--}}
                                    {{--placeholder="{{__('storeDashboard.sePhApproxPriceForTwo')}} {{ config('settings.currencyFormat') }}" required>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <hr>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>{{__('storeDashboard.seLblFullAddress')}}:</label>
                            <div class="col-lg-9">
                                <input value="{{ $restaurant->address }}" type="text" class="form-control form-control-lg" name="address"
                                    placeholder="{{__('storeDashboard.sePhFullAddress')}}" required>
                            </div>
                        </div>
                        {{--<div class="form-group row">--}}
                            {{--<label class="col-lg-3 col-form-label" data-popup="tooltip" title="{{__('storeDashboard.seToolTipPincode')}}" data-placement="bottom">{{__('storeDashboard.seLblPincode')}}:</label>--}}
                            {{--<div class="col-lg-9">--}}
                                {{--<input value="{{ $restaurant->pincode }}" type="text" class="form-control form-control-lg" name="pincode"--}}
                                    {{--placeholder="{{__('storeDashboard.seToolTipPincode')}}">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group row">--}}
                            {{--<label class="col-lg-3 col-form-label">{{__('storeDashboard.seLblLandMark')}}:</label>--}}
                            {{--<div class="col-lg-9">--}}
                                {{--<input value="{{ $restaurant->landmark }}" type="text" class="form-control form-control-lg" name="landmark"--}}
                                    {{--placeholder="{{__('storeDashboard.sePhLandMark')}}">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <fieldset class="gllpLatlonPicker">
                            {{-- <div width="100%" id="map" class="gllpMap" style="position: relative; overflow: hidden;"></div> --}}
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label class="col-form-label">{{__('storeDashboard.seLblLat')}}:</label><input type="text" class="form-control form-control-lg gllpLatitude latitude" value="{{ $restaurant->latitude }}" name="latitude" placeholder="Latitude of the Restaurant" required="required">
                                </div>
                                <div class="col-lg-6">
                                    <label class="col-form-label">{{__('storeDashboard.seLblLong')}}:</label><input type="text" class="form-control form-control-lg gllpLongitude longitude" value="{{ $restaurant->longitude }}" name="longitude" placeholder="Longitude of the Restaurant" required="required">
                                </div>
                            </div>
                            {{-- <input type="hidden" class="gllpZoom" value="20">
                            <div class="d-flex justify-content-center">
                                <div class="col-lg-6 d-flex location-search-block">       
                                    <input type="text" class="form-control form-control-lg gllpSearchField" placeholder="Search for resraurant, city or town...">
                                    <button type="button" class="btn btn-primary gllpSearchButton">{{__('storeDashboard.seLblSearch')}}</button>
                                </div>
                            </div> --}}
                            <span class="text-muted">{{__('storeDashboard.sePhTextLatLong1')}} <a href="https://www.mapcoordinates.net/en" target="_blank">https://www.mapcoordinates.net/en</a></span> <br> {{__('storeDashboard.sePhTextLatLong2')}}
                        </fieldset>
                        <hr>
                        {{--<div class="form-group row">--}}
                            {{--<label class="col-lg-3 col-form-label">{{__('storeDashboard.seLblCertificate')}}:</label>--}}
                            {{--<div class="col-lg-9">--}}
                                {{--<input value="{{ $restaurant->certificate }}" type="text" class="form-control form-control-lg" name="certificate"--}}
                                    {{--placeholder="{{__('storeDashboard.sePhCertificate')}}">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">{{__('storeDashboard.seLblStoreCharge')}}:</label>
                            <div class="col-lg-9">
                                <input value="{{ $restaurant->restaurant_charges }}" type="text" class="form-control form-control-lg restaurant_charges" name="restaurant_charges"
                                    placeholder="{{__('storeDashboard.sePhStoreCharge')}} {{ config('settings.currencyFormat') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">{{__('storeDashboard.seLblPureVeg')}}</label>
                            <div class="col-lg-9">
                                <div class="checkbox checkbox-switchery mt-2">
                                    <label>
                                    <input value="true" type="checkbox" class="switchery-primary" @if($restaurant->is_pureveg) checked="checked" @endif name="is_pureveg">
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">{{__('storeDashboard.seLblMinOrderPrice')}}:</label>
                            <div class="col-lg-9">
                                <input value="{{ $restaurant->min_order_price }}" type="text" class="form-control form-control-lg min_order_price" name="min_order_price"
                                    placeholder="{{__('storeDashboard.sePhMinOrderPrice')}} {{ config('settings.currencyFormat') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">{{ __('storeDashboard.seLblAutomaticScheduling') }}</label>
                            <div class="col-lg-9">
                                <div class="checkbox checkbox-switchery mt-2">
                                    <label>
                                    <input value="true" type="checkbox" class="switchery-primary"
                                    @if($restaurant->is_schedulable) checked="checked" @endif name="is_schedulable">
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                        @csrf
                        <div class="text-left">
                            <div class="btn-group btn-group-justified" style="width: 150px">
                                @if($restaurant->is_active)
                               <a href="{{ route('restaurant.disableRestaurant', $restaurant->id) }}"
                                    class="btn btn-danger btn-labeled btn-labeled-left mr-2" data-popup="tooltip"
                                    title="Users won't be able to place order from this store if Closed" data-placement="bottom">
                                <b><i class="icon-switch2"></i></b>
                                {{__('storeDashboard.seDisable')}}
                                </a>
                                @else
                                <a href="{{ route('restaurant.disableRestaurant', $restaurant->id) }}"
                                    class="btn btn-secondary btn-labeled btn-labeled-left mr-2" data-popup="tooltip"
                                    title="Store is Closed. Open to accept orders." data-placement="bottom">
                                <b><i class="icon-switch2"></i></b>
                                {{__('storeDashboard.seEnable')}}
                                </a>
                                @endif 
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">
                            {{__('storeDashboard.update')}}
                            <i class="icon-database-insert ml-1"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @if($restaurant->is_schedulable)
        <div class="col-md-5">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('restaurant.updateRestaurantScheduleData') }}" method="POST"
                        enctype="multipart/form-data">
                        <legend class="font-weight-semibold text-uppercase font-size-sm">
                            <i class="icon-alarm mr-2"></i> Store Scheduling Times
                        </legend>
                        <div class="form-group row mb-0">
                            <div class="col-lg-4">
                                <h3>{{ __('storeDashboard.seMonday') }}</h3>
                            </div>
                        </div>
                        <!-- Checks if there is any schedule data -->
                        @if(!empty($schedule_data->monday) && count($schedule_data->monday) > 0)
                        <!-- If yes Then Loop Each Data as Time SLots -->
                        @foreach($schedule_data->monday as $time)
                        <div class="form-group row">
                            <div class="col-lg-5">
                                <label class="col-form-label">{{ __('storeDashboard.seOpeningTime') }}</label>
                                <input type="text" class="form-control clock form-control-lg" value="{{$time->open}}"
                                    name="monday[]" required>
                            </div>
                            <div class="col-lg-5">
                                <label class="col-form-label"></span>{{ __('storeDashboard.seClosingTime') }}</label>
                                <input type="text" class="form-control clock form-control-lg" value="{{$time->close}}"
                                    name="monday[]" required>
                            </div>
                            <div class="col-lg-2" day="monday">
                                <label class="col-form-label text-center" style="width: 43px;"></span><i class="icon-circle-down2"></i></label><br>
                                <button class="remove btn btn-danger" data-popup="tooltip" data-placement="right" title="Remove Time Slot">
                                <i class="icon-cross2"></i>
                                </button>
                            </div>
                        </div>
                        @endforeach
                        @endif
                        <div id="monday" class="timeSlots">
                        </div>
                        <a href="javascript:void(0)" onclick="add(this)" data-day="monday" class="btn btn-secondary btn-labeled btn-labeled-left mr-2"> <b><i class="icon-plus22"></i></b>{{ __('storeDashboard.seAddSlot') }}</a>
                        <hr>
                        <div class="form-group row mb-0">
                            <div class="col-lg-4">
                                <h3>{{ __('storeDashboard.seTuesday') }}</h3>
                            </div>
                        </div>
                        <!-- Checks if there is any schedule data -->
                        @if(!empty($schedule_data->tuesday) && count($schedule_data->tuesday) > 0)
                        <!-- If yes Then Loop Each Data as Time SLots -->
                        @foreach($schedule_data->tuesday as $time)
                        <div class="form-group row">
                            <div class="col-lg-5">
                                <label class="col-form-label">{{ __('storeDashboard.seOpeningTime') }}</label>
                                <input type="text" class="form-control clock form-control-lg" value="{{$time->open}}"
                                    name="tuesday[]" required>
                            </div>
                            <div class="col-lg-5">
                                <label class="col-form-label"></span>{{ __('storeDashboard.seClosingTime') }}</label>
                                <input type="text" class="form-control clock form-control-lg" value="{{$time->close}}"
                                    name="tuesday[]" required>
                            </div>
                            <div class="col-lg-2" day="tuesday">
                                <label class="col-form-label text-center" style="width: 43px;"></span><i class="icon-circle-down2"></i></label><br>
                                <button class="remove btn btn-danger" data-popup="tooltip" data-placement="right" title="Remove Time Slot">
                                <i class="icon-cross2"></i>
                                </button>
                            </div>
                        </div>
                        @endforeach
                        @endif
                        <div id="tuesday" class="timeSlots">
                        </div>
                        <a href="javascript:void(0)" onclick="add(this)" data-day="tuesday" class="btn btn-secondary btn-labeled btn-labeled-left mr-2"> <b><i class="icon-plus22"></i></b>{{ __('storeDashboard.seAddSlot') }}</a>
                        <hr>
                        <div class="form-group row mb-0">
                            <div class="col-lg-4">
                                <h3>{{ __('storeDashboard.seWednesday') }}</h3>
                            </div>
                        </div>
                        <!-- Checks if there is any schedule data -->
                        @if(!empty($schedule_data->wednesday) && count($schedule_data->wednesday) > 0)
                        <!-- If yes Then Loop Each Data as Time SLots -->
                        @foreach($schedule_data->wednesday as $time)
                        <div class="form-group row">
                            <div class="col-lg-5">
                                <label class="col-form-label">{{ __('storeDashboard.seOpeningTime') }}</label>
                                <input type="text" class="form-control clock form-control-lg" value="{{$time->open}}"
                                    name="wednesday[]" required>
                            </div>
                            <div class="col-lg-5">
                                <label class="col-form-label"></span>{{ __('storeDashboard.seClosingTime') }}</label>
                                <input type="text" class="form-control clock form-control-lg" value="{{$time->close}}"
                                    name="wednesday[]" required>
                            </div>
                            <div class="col-lg-2" day="wednesday">
                                <label class="col-form-label text-center" style="width: 43px;"></span><i class="icon-circle-down2"></i></label><br>
                                <button class="remove btn btn-danger" data-popup="tooltip" data-placement="right" title="Remove Time Slot">
                                <i class="icon-cross2"></i>
                                </button>
                            </div>
                        </div>
                        @endforeach
                        @endif
                        <div id="wednesday" class="timeSlots">
                        </div>
                        <a href="javascript:void(0)" onclick="add(this)" data-day="wednesday" class="btn btn-secondary btn-labeled btn-labeled-left mr-2"> <b><i class="icon-plus22"></i></b>{{ __('storeDashboard.seAddSlot') }}</a>
                        <hr>
                        <div class="form-group row mb-0">
                            <div class="col-lg-4">
                                <h3>{{ __('storeDashboard.seThursday') }}</h3>
                            </div>
                        </div>
                        <!-- Checks if there is any schedule data -->
                        @if(!empty($schedule_data->thursday) && count($schedule_data->thursday) > 0)
                        <!-- If yes Then Loop Each Data as Time SLots -->
                        @foreach($schedule_data->thursday as $time)
                        <div class="form-group row">
                            <div class="col-lg-5">
                                <label class="col-form-label">{{ __('storeDashboard.seOpeningTime') }}</label>
                                <input type="text" class="form-control clock form-control-lg" value="{{$time->open}}"
                                    name="thursday[]" required>
                            </div>
                            <div class="col-lg-5">
                                <label class="col-form-label"></span>{{ __('storeDashboard.seClosingTime') }}</label>
                                <input type="text" class="form-control clock form-control-lg" value="{{$time->close}}"
                                    name="thursday[]" required>
                            </div>
                            <div class="col-lg-2" day="thursday">
                                <label class="col-form-label text-center" style="width: 43px;"></span><i class="icon-circle-down2"></i></label><br>
                                <button class="remove btn btn-danger" data-popup="tooltip" data-placement="right" title="Remove Time Slot">
                                <i class="icon-cross2"></i>
                                </button>
                            </div>
                        </div>
                        @endforeach
                        @endif
                        <div id="thursday" class="timeSlots">
                        </div>
                        <a href="javascript:void(0)" onclick="add(this)" data-day="thursday" class="btn btn-secondary btn-labeled btn-labeled-left mr-2"> <b><i class="icon-plus22"></i></b>{{ __('storeDashboard.seAddSlot') }}</a>
                        <hr>
                        <div class="form-group row mb-0">
                            <div class="col-lg-4">
                                <h3>{{ __('storeDashboard.seFriday') }}</h3>
                            </div>
                        </div>
                        <!-- Checks if there is any schedule data -->
                        @if(!empty($schedule_data->friday) && count($schedule_data->friday) > 0)
                        <!-- If yes Then Loop Each Data as Time SLots -->
                        @foreach($schedule_data->friday as $time)
                        <div class="form-group row">
                            <div class="col-lg-5">
                                <label class="col-form-label">{{ __('storeDashboard.seOpeningTime') }}</label>
                                <input type="text" class="form-control clock form-control-lg" value="{{$time->open}}"
                                    name="friday[]" required>
                            </div>
                            <div class="col-lg-5">
                                <label class="col-form-label"></span>{{ __('storeDashboard.seClosingTime') }}</label>
                                <input type="text" class="form-control clock form-control-lg" value="{{$time->close}}"
                                    name="friday[]" required>
                            </div>
                            <div class="col-lg-2" day="friday">
                                <label class="col-form-label text-center" style="width: 43px;"></span><i class="icon-circle-down2"></i></label><br>
                                <button class="remove btn btn-danger" data-popup="tooltip" data-placement="right" title="Remove Time Slot">
                                <i class="icon-cross2"></i>
                                </button>
                            </div>
                        </div>
                        @endforeach
                        @endif 
                        <div id="friday" class="timeSlots">
                        </div>
                        <a href="javascript:void(0)" onclick="add(this)" data-day="friday" class="btn btn-secondary btn-labeled btn-labeled-left mr-2"> <b><i class="icon-plus22"></i></b>{{ __('storeDashboard.seAddSlot') }}</a>
                        <hr>
                        <div class="form-group row mb-0">
                            <div class="col-lg-4">
                                <h3>{{ __('storeDashboard.seSaturday') }}</h3>
                            </div>
                        </div>
                        <!-- Checks if there is any schedule data -->
                        @if(!empty($schedule_data->saturday) && count($schedule_data->saturday) > 0)
                        <!-- If yes Then Loop Each Data as Time SLots -->
                        @foreach($schedule_data->saturday as $time)
                        <div class="form-group row">
                            <div class="col-lg-5">
                                <label class="col-form-label">{{ __('storeDashboard.seOpeningTime') }}</label>
                                <input type="text" class="form-control clock form-control-lg" value="{{$time->open}}"
                                    name="saturday[]" required>
                            </div>
                            <div class="col-lg-5">
                                <label class="col-form-label"></span>{{ __('storeDashboard.seClosingTime') }}</label>
                                <input type="text" class="form-control clock form-control-lg" value="{{$time->close}}"
                                    name="saturday[]" required>
                            </div>
                            <div class="col-lg-2" day="saturday">
                                <label class="col-form-label text-center" style="width: 43px;"></span><i class="icon-circle-down2"></i></label><br>
                                <button class="remove btn btn-danger" data-popup="tooltip" data-placement="right" title="Remove Time Slot">
                                <i class="icon-cross2"></i>
                                </button>
                            </div>
                        </div>
                        @endforeach
                        @endif
                        <div id="saturday" class="timeSlots">
                        </div>
                        <a href="javascript:void(0)" onclick="add(this)" data-day="saturday" class="btn btn-secondary btn-labeled btn-labeled-left mr-2"> <b><i class="icon-plus22"></i></b>{{ __('storeDashboard.seAddSlot') }}</a>
                        <hr>
                        <div class="form-group row mb-0">
                            <div class="col-lg-4">
                                <h3>{{ __('storeDashboard.seSunday') }}</h3>
                            </div>
                        </div>
                        <!-- Checks if there is any schedule data -->
                        @if(!empty($schedule_data->sunday) && count($schedule_data->sunday) > 0)
                        <!-- If yes Then Loop Each Data as Time SLots -->
                        @foreach($schedule_data->sunday as $time)
                        <div class="form-group row">
                            <div class="col-lg-5">
                                <label class="col-form-label">{{ __('storeDashboard.seOpeningTime') }}</label>
                                <input type="text" class="form-control clock form-control-lg" value="{{$time->open}}"
                                    name="sunday[]" required>
                            </div>
                            <div class="col-lg-5">
                                <label class="col-form-label"></span>{{ __('storeDashboard.seClosingTime') }}</label>
                                <input type="text" class="form-control clock form-control-lg" value="{{$time->close}}"
                                    name="sunday[]" required>
                            </div>
                            <div class="col-lg-2" day="sunday">
                                <label class="col-form-label text-center" style="width: 43px;"></span><i class="icon-circle-down2"></i></label><br>
                                <button class="remove btn btn-danger" data-popup="tooltip" data-placement="right" title="Remove Time Slot">
                                <i class="icon-cross2"></i>
                                </button>
                            </div>
                        </div>
                        @endforeach
                        @endif
                        <div id="sunday" class="timeSlots">
                        </div>
                        <a href="javascript:void(0)" onclick="add(this)" data-day="sunday" class="btn btn-secondary btn-labeled btn-labeled-left mr-2"> <b><i class="icon-plus22"></i></b>{{ __('storeDashboard.seAddSlot') }}</a>
                        <hr>
                        <input type="text" name="restaurant_id" hidden value="{{$restaurant->id}}">
                        @csrf
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary" data-popup="tooltip" title="{{ __('storeDashboard.seScheduleUpdateMsg') }}" data-placement="bottom">
                            {{ __('storeDashboard.update') }}
                            <i class="icon-database-insert ml-1"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
<script>
    function readURL2(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function (e) {
                $('.slider-preview-image2')
                    .removeClass('hidden')
                    .attr('src', e.target.result)
                    .width(160)
                    .height(117)
                    .css('borderRadius', '0.275rem');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function (e) {
                $('.slider-preview-image')
                    .removeClass('hidden')
                    .attr('src', e.target.result)
                    .width(160)
                   .height(117)
                   .css('borderRadius', '0.275rem');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
     function add(data) {
        var para = document.createElement("div");
        let day = data.getAttribute("data-day")
        para.innerHTML ="<div class='form-group row'> <div class='col-lg-5'><label class='col-form-label'>{{ __('storeDashboard.seOpeningTime') }}</label><input type='text' class='form-control clock form-control-lg' name='"+day+"[]' required> </div> <div class='col-lg-5'> <label class='col-form-label'>{{ __('storeDashboard.seClosingTime') }}</label><input type='text' class='form-control clock form-control-lg' name='"+day+"[]'  required> </div> <div class='col-lg-2'> <label class='col-form-label text-center' style='width: 43px'></span><i class='icon-circle-down2'></i></label><br><button class='remove btn btn-danger' data-popup='tooltip' data-placement='right' title='{{ __('storeDashboard.seRemoveTimeSlot') }}'><i class='icon-cross2'></i></button></div></div>";
        document.getElementById(day).appendChild(para);
        $('.clock').bootstrapMaterialDatePicker({
            shortTime: true,
            date: false,
            time: true,
            format: 'HH:mm'
        });
    }
    $(function () {

        $('body').tooltip({
            selector: 'button'
        });
        
        $('.clock').bootstrapMaterialDatePicker({
            shortTime: true,
            date: false,
            time: true,
            format: 'HH:mm'
        });
        $(document).on("click", ".remove", function() {
            $(this).tooltip('hide')
            $(this).parent().parent().remove();
        });
        
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });
    
         if (Array.prototype.forEach) {
               var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery-primary'));
               elems.forEach(function(html) {
                   var switchery = new Switchery(html, { color: '#2196F3' });
               });
           }
           else {
               var elems = document.querySelectorAll('.switchery-primary');
               for (var i = 0; i < elems.length; i++) {
                   var switchery = new Switchery(elems[i], { color: '#2196F3' });
               }
           }
    
       $('.form-control-uniform').uniform();

       $('.delivery_time').numeric({allowThouSep:false});
       $('.price_range').numeric({allowThouSep:false});
       $('.latitude').numeric({allowThouSep:false});
       $('.longitude').numeric({allowThouSep:false});
       $('.restaurant_charges').numeric({ allowThouSep:false, maxDecimalPlaces: 2 });
       $('.delivery_charges').numeric({ allowThouSep:false, maxDecimalPlaces: 2 });
       $('.min_order_price').numeric({ allowThouSep:false, maxDecimalPlaces: 2, allowMinus: false });
    });
</script>
@endsection