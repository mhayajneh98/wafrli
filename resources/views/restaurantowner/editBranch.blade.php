@extends('admin.layouts.master')
@section("title") Edit Branch - Store Dashboard
@endsection
@section('content')
    <style>
        .delivery-div {
            background-color: #fafafa;
            padding: 1rem;
        }
        .location-search-block {
            position: relative;
            top: -26rem;
            z-index: 999;
        }
        hr {
            border-top: 1px solid #f2f2f2 !important;
        }
    </style>
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-circle-right2 mr-2"></i>
                    <span class="font-weight-bold mr-2">Editing</span>
                    <span class="badge badge-primary badge-pill animated flipInX">"{{ $branch->name }}"</span>
                </h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('restaurant.updateBranch') }}" method="POST" enctype="multipart/form-data">
                            <legend class="font-weight-semibold text-uppercase font-size-sm">
                                <i class="icon-store2 mr-2"></i> Branch Details
                            </legend>
                            <input type="hidden" name="id" value="{{ $branch->id }}">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Branch Name:</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control form-control-lg" value="{{$branch->name}}" name="name" placeholder="Branch Name"
                                           required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Area:</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control form-control-lg" value="{{$branch->area}}" name="area"
                                           placeholder="Branch Area" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Phone Number:</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control form-control-lg" value="{{$branch->phone_number}}" name="phone_number"
                                           placeholder="Phone Number" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Restaurants:</label>
                                <div class="col-lg-9">
                                    <select class="form-control select-search select" name="restaurant_id" required>
                                        <option value="0" class="text-capitalize" selected="selected">ALL STORES</option>
                                        @foreach ($restaurants as $restaurant)
                                            <option @if($branch->restaurants_id == $restaurant->id) {{"SELECTED"}} @endif value="{{ $restaurant->id }}" class="text-capitalize">{{ $restaurant->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <fieldset class="gllpLatlonPicker">
                                <div width="100%" id="map" class="gllpMap" style="position: relative; overflow: hidden;"></div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="col-form-label">Latitude:</label><input type="text"
                                                                                              class="form-control form-control-lg gllpLatitude latitude" value="{{$branch->latitude}}"
                                                                                              name="latitude" placeholder="Latitude of the Store" required="required">
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="col-form-label">Longitude:</label><input type="text"
                                                                                               class="form-control form-control-lg gllpLongitude longitude" value="{{$branch->longitude}}"
                                                                                               name="longitude" placeholder="Longitude of the Store" required="required">
                                    </div>
                                </div>
                                <span class="text-muted">You can use services like: <a
                                            href="https://www.mapcoordinates.net/en" target="_blank">https://www.mapcoordinates.net/en</a></span>
                                <br> If you enter an invalid Latitude/Longitude the map system might crash with a white screen.
                            </fieldset>
                            @csrf
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">
                                    UPDATE
                                    <i class="icon-database-insert ml-1"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection