@extends('admin.layouts.master')
@section("title") Branches - Store Dashboard
@endsection
@section('content')
    <style>
        .delivery-div {
            background-color: #fafafa;
            padding: 1rem;
        }

        .location-search-block {
            position: relative;
            top: -26rem;
            z-index: 999;
        }
    </style>
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-circle-right2 mr-2"></i>
                    @if(empty($query))
                        <span class="font-weight-bold mr-2">TOTAL</span>
                        <span class="badge badge-primary badge-pill animated flipInX">{{ $count }}</span>
                    @else
                        <span class="font-weight-bold mr-2">TOTAL</span>
                        <span class="badge badge-primary badge-pill animated flipInX mr-2">{{ $count }}</span>
                        <span class="font-weight-bold mr-2">Results for "{{ $query }}"</span>
                    @endif
                </h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            <div class="header-elements d-none py-0 mb-3 mb-md-0">
                <div class="breadcrumb">
                    @if(Request::is('store-owner/branches'))
                        <button type="button" class="btn btn-secondary btn-labeled btn-labeled-left mr-2" id="addNewBranch"
                                data-toggle="modal" data-target="#addNewBranchModal">
                            <b><i class="icon-plus2"></i></b>
                            Add New Branch
                        </button>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="content">

        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Area</th>
                            <th style="width: 15%">Created At</th>
                            <th class="text-center" style="width: 10%;"><i class="
                                icon-circle-down2"></i></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($branches as $branch)
                            <tr>
                                <td>{{ $branch->id }}</td>
                                <td>{{ $branch->name }}</td>


                                <td>{{ $branch->created_at->diffForHumans() }}</td>
                                <td class="text-center">
                                    <div class="btn-group btn-group-justified align-items-center">
                                        <a href="{{ route('restaurant.get.editBranch', $branch->id) }}"
                                           class="badge badge-primary badge-icon"> Edit <i
                                                    class="icon-database-edit2 ml-1"></i></a>
                                        <div class="checkbox checkbox-switchery ml-1" style="padding-top: 0.8rem;">
                                            <label>
                                                <input value="true" type="checkbox" class="action-switch"
                                                       @if($branch->is_active) checked="checked" @endif data-id="{{ $branch->id }}">
                                            </label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="mt-3">
                        {{ $branches->appends($_GET)->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="addNewBranchModal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><span class="font-weight-bold">Add New Branch</span></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('restaurant.saveNewBranch') }}" method="POST" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Branch Name:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control form-control-lg" name="name" placeholder="Branch Name"
                                       required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Area:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control form-control-lg" name="area"
                                       placeholder="Branch Area" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Phone Number:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control form-control-lg" name="phone_number"
                                       placeholder="Phone Number" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Restaurants:</label>
                            <div class="col-lg-9">
                                <select class="form-control select-search select" name="restaurant_id" required>
                                    <option value="0" class="text-capitalize" selected="selected">ALL STORES</option>
                                    @foreach ($restaurants as $restaurant)
                                        <option value="{{ $restaurant->id }}" class="text-capitalize">{{ $restaurant->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <fieldset class="gllpLatlonPicker">
                            <div width="100%" id="map" class="gllpMap" style="position: relative; overflow: hidden;"></div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label class="col-form-label">Latitude:</label><input type="text"
                                                                                          class="form-control form-control-lg gllpLatitude latitude" value="40.6976701"
                                                                                          name="latitude" placeholder="Latitude of the Store" required="required">
                                </div>
                                <div class="col-lg-6">
                                    <label class="col-form-label">Longitude:</label><input type="text"
                                                                                           class="form-control form-control-lg gllpLongitude longitude" value="-74.2598672"
                                                                                           name="longitude" placeholder="Longitude of the Store" required="required">
                                </div>
                            </div>
                            <span class="text-muted">You can use services like: <a
                                        href="https://www.mapcoordinates.net/en" target="_blank">https://www.mapcoordinates.net/en</a></span>
                            <br> If you enter an invalid Latitude/Longitude the map system might crash with a white screen.
                        </fieldset>
                        @csrf
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">
                                SAVE
                                <i class="icon-database-insert ml-1"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                let reader = new FileReader();
                reader.onload = function (e) {
                    $('.slider-preview-image')
                        .removeClass('hidden')
                        .attr('src', e.target.result)
                        .width(120)
                        .height(120);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(function () {
            $('.summernote-editor').summernote({
                height: 200,
                popover: {
                    image: [],
                    link: [],
                    air: []
                }
            });

            $('.select').select2();

            var recommendeditem = document.querySelector('.recommendeditem');
            new Switchery(recommendeditem, { color: '#f44336' });

            var popularitem = document.querySelector('.popularitem');
            new Switchery(popularitem, { color: '#8360c3' });

            var newitem = document.querySelector('.newitem');
            new Switchery(newitem, { color: '#333' });

            var vegitem = document.querySelector('.vegitem');
            new Switchery(vegitem, { color: '#008000' });

            $('.form-control-uniform').uniform();

            $('#downloadSampleItemCsv').click(function(event) {
                event.preventDefault();
                window.location.href = "/assets/docs/items-sample-csv.csv";
            });
            $('.price').numeric({allowThouSep:false, maxDecimalPlaces: 2 });

            //Switch Action Function
            if (Array.prototype.forEach) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.action-switch'));
                elems.forEach(function(html) {
                    var switchery = new Switchery(html, { color: '#8360c3' });
                });
            }
            else {
                var elems = document.querySelectorAll('.action-switch');
                for (var i = 0; i < elems.length; i++) {
                    var switchery = new Switchery(elems[i], { color: '#8360c3' });
                }
            }

            $('.action-switch').click(function(event) {
                let id = $(this).attr("data-id")
                let url = "{{ url('/store-owner/branch/disable/') }}/"+id;
                window.location.href = url;
            });
        });
    </script>
@endsection