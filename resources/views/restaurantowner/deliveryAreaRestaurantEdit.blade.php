@extends('admin.layouts.master')
@section("title") Edit Delivery Area Restaurant - Dashboard
@endsection
@section('content')
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-circle-right2 mr-2"></i>
                    <span class="font-weight-bold mr-2">Editing</span>
                </h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('restaurant.updateDeliveryAreaRestaurant') }}" method="POST" enctype="multipart/form-data">
                        <legend class="font-weight-semibold text-uppercase font-size-sm">
                            <i class="icon-address-book mr-2"></i> Restaurant Delivery Area
                        </legend>
                        <input type="hidden" name="id" value="{{ $item->id }}">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Restaurant:</label>
                            <div class="col-lg-9">
                                <select class="form-control select-search" name="restaurant_id" required>
                                    @foreach ($restaurants as $restaurant)
                                        <option value="{{ $restaurant->id }}" @if($item->restaurant_id == $restaurant->id ) {{"SELECTED"}} @endif class="text-capitalize">{{ $restaurant->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Delivery Areas:</label>
                            <div class="col-lg-9">
                                <select class="form-control select-search" name="delivery_area_id" required>
                                    @foreach ($deliveryAreas as $deliveryArea)
                                        <option value="{{ $deliveryArea->id }}" @if($item->delivery_area_id == $deliveryArea->id ) {{"SELECTED"}} @endif  class="text-capitalize" >{{ $deliveryArea->area_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Price:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control form-control-lg" value="{{$item->price}}" name="price"
                                       placeholder="Price" required>
                            </div>
                        </div>
                        @csrf
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">
                                UPDATE
                                <i class="icon-database-insert ml-1"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection