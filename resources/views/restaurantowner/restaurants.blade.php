@extends('admin.layouts.master')
@section("title") {{__('storeDashboard.spPageTitle')}}
@endsection
@section('content')
<style>
    .delivery-div {
        background-color: #fafafa;
        padding: 1rem;
    }
    .location-search-block {
        position: relative;
        top: -26rem;
        z-index: 999;
    }
</style>
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-circle-right2 mr-2"></i>
                <span class="font-weight-bold mr-2">{{__('storeDashboard.total')}}</span>
                <span class="badge badge-primary badge-pill animated flipInX">{{ count($restaurants) }}</span>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        <div class="header-elements d-none py-0 mb-3 mb-md-0">
            <div class="breadcrumb">
                <button type="button" class="btn btn-secondary btn-labeled btn-labeled-left mr-2" id="addNewRestaurant"
                    data-toggle="modal" data-target="#addNewRestaurantModal">
                <b><i class="icon-plus2"></i></b>
                {{__('storeDashboard.spAddNewStoreBtn')}}
                </button>
            </div>
        </div>
    </div>
</div>
<div class="content">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>{{__('storeDashboard.spTableID')}}</th>
                            <th>{{__('storeDashboard.spTableImage')}}</th>
                            <th>{{__('storeDashboard.spTableName')}}</th>
                            <th>{{__('storeDashboard.spTableAddress')}}</th>
                            <th>{{__('storeDashboard.spTableStatus')}}</th>
                            <th style="width: 15%">{{__('storeDashboard.spTableCA')}}</th>
                            <th class="text-center" style="width: 10%;"><i class="
                                icon-circle-down2"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($restaurants as $restaurant)
                        <tr>
                            <td>{{ $restaurant->id }}</td>
                            <td><img src="{{ $restaurant->image }}" alt="{{ $restaurant->name }}" height="80" width="80" style="border-radius: 0.275rem;"></td>
                            <td>{{ $restaurant->name }}</td>
                            <td>{{ $restaurant->address }}</td>
                            <td>
                                @if(!$restaurant->is_accepted)
                                <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                {{__('storeDashboard.spRowPending')}}
                                </span>
                                @endif
                                <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                @if($restaurant->is_active) {{__('storeDashboard.spRowActive')}} @else {{__('storeDashboard.spRowInActive')}} @endif
                                </span>
                            </td>
                            <td>{{ $restaurant->created_at->diffForHumans() }}</td>
                            <td class="text-center">
                                <div class="btn-group btn-group-justified">
                                    <a href="{{ route('restaurant.get.editRestaurant', $restaurant->id) }}"
                                        class="badge badge-primary badge-icon"> {{__('storeDashboard.edit')}} <i
                                        class="icon-database-edit2 ml-1"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="addNewRestaurantModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="font-weight-bold">{{__('storeDashboard.spAddNewStoreBtn')}}</span></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('restaurant.saveNewRestaurant') }}" method="POST" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>{{__('storeDashboard.seLblStoreName')}}:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg" name="name"
                                placeholder="{{__('storeDashboard.sePhStoreName')}}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>{{__('storeDashboard.seLblDescription')}}:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg" name="description"
                                placeholder="{{__('storeDashboard.sePhDescription')}}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Cover Image:</label>
                        <div class="col-lg-9">
                            <img class="slider-preview-image hidden" />
                            <div class="uploader">
                                <input type="file" class="form-control-lg form-control-uniform" name="cover_image" required
                                       accept="image/x-png,image/gif,image/jpeg" onchange="readURL(this);">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>{{__('storeDashboard.seLblImage')}}:</label>
                        <div class="col-lg-9">
                            <img class="slider-preview-image2 hidden"/>
                            <div class="uploader">
                                <input type="file" class="form-control-lg form-control-uniform" name="image" required accept="image/x-png,image/gif,image/jpeg" onchange="readURL2(this);">
                                <span class="help-text text-muted">{{__('storeDashboard.sePhImage')}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>{{__('storeDashboard.seLblApproxDeliveryTime')}}:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg delivery_time" name="delivery_time"
                                placeholder="{{__('storeDashboard.sePhApproxDeliveryTime')}}" required>
                        </div>
                    </div>
                    {{--<div class="form-group row">--}}
                        {{--<label class="col-lg-3 col-form-label"><span class="text-danger">*</span>{{__('storeDashboard.seLblApproxPriceForTwo')}}:</label>--}}
                        {{--<div class="col-lg-9">--}}
                            {{--<input type="text" class="form-control form-control-lg price_range" name="price_range"--}}
                                {{--placeholder="{{__('storeDashboard.sePhApproxPriceForTwo')}} {{ config('settings.currencyFormat') }}" required>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Payment Method:</label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" name="payment_method" required>
                                <option value="1" class="text-capitalize">Cash</option>
                                <option value="2" class="text-capitalize">Online Gateway</option>
                                <option value="3" class="text-capitalize">Both Cash & Online Gateway</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Currency:</label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" name="currency" required>
                                <option value="AED" @if(config('settings.currencyId')=="AED" ) selected
                                        @endif>AED</option>
                                <option value="AFN" @if(config('settings.currencyId')=="AFN" ) selected
                                        @endif>AFN</option>
                                <option value="ALL" @if(config('settings.currencyId')=="ALL" ) selected
                                        @endif>ALL</option>
                                <option value="AMD" @if(config('settings.currencyId')=="AMD" ) selected
                                        @endif>AMD</option>
                                <option value="ANG" @if(config('settings.currencyId')=="ANG" ) selected
                                        @endif>ANG</option>
                                <option value="ANG" @if(config('settings.currencyId')=="ANG" ) selected
                                        @endif>ANG</option>
                                <option value="AOA" @if(config('settings.currencyId')=="AOA" ) selected
                                        @endif>AOA</option>
                                <option value="ARS" @if(config('settings.currencyId')=="ARS" ) selected
                                        @endif>ARS</option>
                                <option value="AUD" @if(config('settings.currencyId')=="AUD" ) selected
                                        @endif>AUD</option>
                                <option value="AWG" @if(config('settings.currencyId')=="AWG" ) selected
                                        @endif>AWG</option>
                                <option value="AZN" @if(config('settings.currencyId')=="AZN" ) selected
                                        @endif>AZN</option>
                                <option value="BAM" @if(config('settings.currencyId')=="BAM" ) selected
                                        @endif>BAM</option>
                                <option value="BBD" @if(config('settings.currencyId')=="BBD" ) selected
                                        @endif>BBD</option>
                                <option value="BDT" @if(config('settings.currencyId')=="BDT" ) selected
                                        @endif>BDT</option>
                                <option value="BGN" @if(config('settings.currencyId')=="BGN" ) selected
                                        @endif>BGN</option>
                                <option value="BHD" @if(config('settings.currencyId')=="BHD" ) selected
                                        @endif>BHD</option>
                                <option value="BIF" @if(config('settings.currencyId')=="BIF" ) selected
                                        @endif>BIF</option>
                                <option value="BMD" @if(config('settings.currencyId')=="BMD" ) selected
                                        @endif>BMD</option>
                                <option value="BND" @if(config('settings.currencyId')=="BND" ) selected
                                        @endif>BND</option>
                                <option value="BOB" @if(config('settings.currencyId')=="BOB" ) selected
                                        @endif>BOB</option>
                                <option value="BOV" @if(config('settings.currencyId')=="BOV" ) selected
                                        @endif>BOV</option>
                                <option value="BRL" @if(config('settings.currencyId')=="BRL" ) selected
                                        @endif>BRL</option>
                                <option value="BSD" @if(config('settings.currencyId')=="BSD" ) selected
                                        @endif>BSD</option>
                                <option value="BTN" @if(config('settings.currencyId')=="BTN" ) selected
                                        @endif>BTN</option>
                                <option value="BWP" @if(config('settings.currencyId')=="BWP" ) selected
                                        @endif>BWP</option>
                                <option value="BYN" @if(config('settings.currencyId')=="BYN" ) selected
                                        @endif>BYN</option>
                                <option value="BZD" @if(config('settings.currencyId')=="BZD" ) selected
                                        @endif>BZD</option>
                                <option value="CAD" @if(config('settings.currencyId')=="CAD" ) selected
                                        @endif>CAD</option>
                                <option value="CDF" @if(config('settings.currencyId')=="CDF" ) selected
                                        @endif>CDF</option>
                                <option value="CHE" @if(config('settings.currencyId')=="CHE" ) selected
                                        @endif>CHE</option>
                                <option value="CHF" @if(config('settings.currencyId')=="CHF" ) selected
                                        @endif>CHF</option>
                                <option value="CHW" @if(config('settings.currencyId')=="CHW" ) selected
                                        @endif>CHW</option>
                                <option value="CLF" @if(config('settings.currencyId')=="CLF" ) selected
                                        @endif>CLF</option>
                                <option value="CLP" @if(config('settings.currencyId')=="CLP" ) selected
                                        @endif>CLP</option>
                                <option value="CNY" @if(config('settings.currencyId')=="CNY" ) selected
                                        @endif>CNY</option>
                                <option value="COP" @if(config('settings.currencyId')=="COP" ) selected
                                        @endif>COP</option>
                                <option value="COU" @if(config('settings.currencyId')=="COU" ) selected
                                        @endif>COU</option>
                                <option value="CRC" @if(config('settings.currencyId')=="CRC" ) selected
                                        @endif>CRC</option>
                                <option value="CUC" @if(config('settings.currencyId')=="CUC" ) selected
                                        @endif>CUC</option>
                                <option value="CVE" @if(config('settings.currencyId')=="CVE" ) selected
                                        @endif>CVE</option>
                                <option value="CZK" @if(config('settings.currencyId')=="CZK" ) selected
                                        @endif>CZK</option>
                                <option value="DJF" @if(config('settings.currencyId')=="DJF" ) selected
                                        @endif>DJF</option>
                                <option value="DKK" @if(config('settings.currencyId')=="DKK" ) selected
                                        @endif>DKK</option>
                                <option value="DOP" @if(config('settings.currencyId')=="DOP" ) selected
                                        @endif>DOP</option>
                                <option value="DZD" @if(config('settings.currencyId')=="DZD" ) selected
                                        @endif>DZD</option>
                                <option value="EGP" @if(config('settings.currencyId')=="EGP" ) selected
                                        @endif>EGP</option>
                                <option value="ERN" @if(config('settings.currencyId')=="ERN" ) selected
                                        @endif>ERN</option>
                                <option value="ETB" @if(config('settings.currencyId')=="ETB" ) selected
                                        @endif>ETB</option>
                                <option value="EUR" @if(config('settings.currencyId')=="EUR" ) selected
                                        @endif>EUR</option>
                                <option value="FJD" @if(config('settings.currencyId')=="FJD" ) selected
                                        @endif>FJD</option>
                                <option value="FKP" @if(config('settings.currencyId')=="FKP" ) selected
                                        @endif>FKP</option>
                                <option value="GBP" @if(config('settings.currencyId')=="GBP" ) selected
                                        @endif>GBP</option>
                                <option value="GEL" @if(config('settings.currencyId')=="GEL" ) selected
                                        @endif>GEL</option>
                                <option value="GHS" @if(config('settings.currencyId')=="GHS" ) selected
                                        @endif>GHS</option>
                                <option value="GIP" @if(config('settings.currencyId')=="GIP" ) selected
                                        @endif>GIP</option>
                                <option value="GMD" @if(config('settings.currencyId')=="GMD" ) selected
                                        @endif>GMD</option>
                                <option value="GNF" @if(config('settings.currencyId')=="GNF" ) selected
                                        @endif>GNF</option>
                                <option value="GTQ" @if(config('settings.currencyId')=="GTQ" ) selected
                                        @endif>GTQ</option>
                                <option value="GYD" @if(config('settings.currencyId')=="GYD" ) selected
                                        @endif>GYD</option>
                                <option value="HKD" @if(config('settings.currencyId')=="HKD" ) selected
                                        @endif>HKD</option>
                                <option value="HNL" @if(config('settings.currencyId')=="HNL" ) selected
                                        @endif>HNL</option>
                                <option value="HRK" @if(config('settings.currencyId')=="HRK" ) selected
                                        @endif>HRK</option>
                                <option value="HTG" @if(config('settings.currencyId')=="HTG" ) selected
                                        @endif>HTG</option>
                                <option value="HUF" @if(config('settings.currencyId')=="HUF" ) selected
                                        @endif>HUF</option>
                                <option value="IDR" @if(config('settings.currencyId')=="IDR" ) selected
                                        @endif>IDR</option>
                                <option value="ILS" @if(config('settings.currencyId')=="ILS" ) selected
                                        @endif>ILS</option>
                                <option value="INR" @if(config('settings.currencyId')=="INR" ) selected
                                        @endif>INR</option>
                                <option value="IQD" @if(config('settings.currencyId')=="IQD" ) selected
                                        @endif>IQD</option>
                                <option value="IRR" @if(config('settings.currencyId')=="IRR" ) selected
                                        @endif>IRR</option>
                                <option value="ISK" @if(config('settings.currencyId')=="ISK" ) selected
                                        @endif>ISK</option>
                                <option value="JMD" @if(config('settings.currencyId')=="JMD" ) selected
                                        @endif>JMD</option>
                                <option value="JOD" @if(config('settings.currencyId')=="JOD" ) selected
                                        @endif>JOD</option>
                                <option value="JPY" @if(config('settings.currencyId')=="JPY" ) selected
                                        @endif>JPY</option>
                                <option value="KES" @if(config('settings.currencyId')=="KES" ) selected
                                        @endif>KES</option>
                                <option value="KGS" @if(config('settings.currencyId')=="KGS" ) selected
                                        @endif>KGS</option>
                                <option value="KHR" @if(config('settings.currencyId')=="KHR" ) selected
                                        @endif>KHR</option>
                                <option value="KMF" @if(config('settings.currencyId')=="KMF" ) selected
                                        @endif>KMF</option>
                                <option value="KPW" @if(config('settings.currencyId')=="KPW" ) selected
                                        @endif>KPW</option>
                                <option value="KRW" @if(config('settings.currencyId')=="KRW" ) selected
                                        @endif>KRW</option>
                                <option value="KWD" @if(config('settings.currencyId')=="KWD" ) selected
                                        @endif>KWD</option>
                                <option value="KYD" @if(config('settings.currencyId')=="KYD" ) selected
                                        @endif>KYD</option>
                                <option value="KZT" @if(config('settings.currencyId')=="KZT" ) selected
                                        @endif>KZT</option>
                                <option value="LAK" @if(config('settings.currencyId')=="LAK" ) selected
                                        @endif>LAK</option>
                                <option value="LBP" @if(config('settings.currencyId')=="LBP" ) selected
                                        @endif>LBP</option>
                                <option value="LKR" @if(config('settings.currencyId')=="LKR" ) selected
                                        @endif>LKR</option>
                                <option value="LRD" @if(config('settings.currencyId')=="LRD" ) selected
                                        @endif>LRD</option>
                                <option value="LSL" @if(config('settings.currencyId')=="LSL" ) selected
                                        @endif>LSL</option>
                                <option value="LYD" @if(config('settings.currencyId')=="LYD" ) selected
                                        @endif>LYD</option>
                                <option value="MAD" @if(config('settings.currencyId')=="MAD" ) selected
                                        @endif>MAD</option>
                                <option value="MDL" @if(config('settings.currencyId')=="MDL" ) selected
                                        @endif>MDL</option>
                                <option value="MGA" @if(config('settings.currencyId')=="MGA" ) selected
                                        @endif>MGA</option>
                                <option value="MKD" @if(config('settings.currencyId')=="MKD" ) selected
                                        @endif>MKD</option>
                                <option value="MMK" @if(config('settings.currencyId')=="MMK" ) selected
                                        @endif>MMK</option>
                                <option value="MNT" @if(config('settings.currencyId')=="MNT" ) selected
                                        @endif>MNT</option>
                                <option value="MOP" @if(config('settings.currencyId')=="MOP" ) selected
                                        @endif>MOP</option>
                                <option value="MRU" @if(config('settings.currencyId')=="MRU" ) selected
                                        @endif>MRU</option>
                                <option value="MUR" @if(config('settings.currencyId')=="MUR" ) selected
                                        @endif>MUR</option>
                                <option value="MVR" @if(config('settings.currencyId')=="MVR" ) selected
                                        @endif>MVR</option>
                                <option value="MWK" @if(config('settings.currencyId')=="MWK" ) selected
                                        @endif>MWK</option>
                                <option value="MXN" @if(config('settings.currencyId')=="MXN" ) selected
                                        @endif>MXN</option>
                                <option value="MXV" @if(config('settings.currencyId')=="MXV" ) selected
                                        @endif>MXV</option>
                                <option value="MYR" @if(config('settings.currencyId')=="MYR" ) selected
                                        @endif>MYR</option>
                                <option value="MZN" @if(config('settings.currencyId')=="MZN" ) selected
                                        @endif>MZN</option>
                                <option value="NAD" @if(config('settings.currencyId')=="NAD" ) selected
                                        @endif>NAD</option>
                                <option value="NGN" @if(config('settings.currencyId')=="NGN" ) selected
                                        @endif>NGN</option>
                                <option value="NIO" @if(config('settings.currencyId')=="NIO" ) selected
                                        @endif>NIO</option>
                                <option value="NOK" @if(config('settings.currencyId')=="NOK" ) selected
                                        @endif>NOK</option>
                                <option value="NPR" @if(config('settings.currencyId')=="NPR" ) selected
                                        @endif>NPR</option>
                                <option value="NZD" @if(config('settings.currencyId')=="NZD" ) selected
                                        @endif>NZD</option>
                                <option value="OMR" @if(config('settings.currencyId')=="OMR" ) selected
                                        @endif>OMR</option>
                                <option value="PAB" @if(config('settings.currencyId')=="PAB" ) selected
                                        @endif>PAB</option>
                                <option value="PEN" @if(config('settings.currencyId')=="PEN" ) selected
                                        @endif>PEN</option>
                                <option value="PGK" @if(config('settings.currencyId')=="PGK" ) selected
                                        @endif>PGK</option>
                                <option value="PHP" @if(config('settings.currencyId')=="PHP" ) selected
                                        @endif>PHP</option>
                                <option value="PKR" @if(config('settings.currencyId')=="PKR" ) selected
                                        @endif>PKR</option>
                                <option value="PLN" @if(config('settings.currencyId')=="PLN" ) selected
                                        @endif>PLN</option>
                                <option value="PYG" @if(config('settings.currencyId')=="PYG" ) selected
                                        @endif>PYG</option>
                                <option value="QAR" @if(config('settings.currencyId')=="QAR" ) selected
                                        @endif>QAR</option>
                                <option value="RON" @if(config('settings.currencyId')=="RON" ) selected
                                        @endif>RON</option>
                                <option value="RSD" @if(config('settings.currencyId')=="RSD" ) selected
                                        @endif>RSD</option>
                                <option value="RUB" @if(config('settings.currencyId')=="RUB" ) selected
                                        @endif>RUB</option>
                                <option value="RWF" @if(config('settings.currencyId')=="RWF" ) selected
                                        @endif>RWF</option>
                                <option value="SAR" @if(config('settings.currencyId')=="SAR" ) selected
                                        @endif>SAR</option>
                                <option value="SBD" @if(config('settings.currencyId')=="SBD" ) selected
                                        @endif>SBD</option>
                                <option value="SCR" @if(config('settings.currencyId')=="SCR" ) selected
                                        @endif>SCR</option>
                                <option value="SDG" @if(config('settings.currencyId')=="SDG" ) selected
                                        @endif>SDG</option>
                                <option value="SEK" @if(config('settings.currencyId')=="SEK" ) selected
                                        @endif>SEK</option>
                                <option value="SGD" @if(config('settings.currencyId')=="SGD" ) selected
                                        @endif>SGD</option>
                                <option value="SHP" @if(config('settings.currencyId')=="SHP" ) selected
                                        @endif>SHP</option>
                                <option value="SLL" @if(config('settings.currencyId')=="SLL" ) selected
                                        @endif>SLL</option>
                                <option value="SOS" @if(config('settings.currencyId')=="SOS" ) selected
                                        @endif>SOS</option>
                                <option value="SRD" @if(config('settings.currencyId')=="SRD" ) selected
                                        @endif>SRD</option>
                                <option value="SSP" @if(config('settings.currencyId')=="SSP" ) selected
                                        @endif>SSP</option>
                                <option value="STN" @if(config('settings.currencyId')=="STN" ) selected
                                        @endif>STN</option>
                                <option value="SVC" @if(config('settings.currencyId')=="SVC" ) selected
                                        @endif>SVC</option>
                                <option value="SYP" @if(config('settings.currencyId')=="SYP" ) selected
                                        @endif>SYP</option>
                                <option value="SZL" @if(config('settings.currencyId')=="SZL" ) selected
                                        @endif>SZL</option>
                                <option value="THB" @if(config('settings.currencyId')=="THB" ) selected
                                        @endif>THB</option>
                                <option value="TJS" @if(config('settings.currencyId')=="TJS" ) selected
                                        @endif>TJS</option>
                                <option value="TMT" @if(config('settings.currencyId')=="TMT" ) selected
                                        @endif>TMT</option>
                                <option value="TND" @if(config('settings.currencyId')=="TND" ) selected
                                        @endif>TND</option>
                                <option value="TOP" @if(config('settings.currencyId')=="TOP" ) selected
                                        @endif>TOP</option>
                                <option value="TRY" @if(config('settings.currencyId')=="TRY" ) selected
                                        @endif>TRY</option>
                                <option value="TTD" @if(config('settings.currencyId')=="TTD" ) selected
                                        @endif>TTD</option>
                                <option value="TWD" @if(config('settings.currencyId')=="TWD" ) selected
                                        @endif>TWD</option>
                                <option value="TZS" @if(config('settings.currencyId')=="TZS" ) selected
                                        @endif>TZS</option>
                                <option value="UAH" @if(config('settings.currencyId')=="UAH" ) selected
                                        @endif>UAH</option>
                                <option value="UGX" @if(config('settings.currencyId')=="UGX" ) selected
                                        @endif>UGX</option>
                                <option value="USD" @if(config('settings.currencyId')=="USD" ) selected
                                        @endif>USD</option>
                                <option value="USN" @if(config('settings.currencyId')=="USN" ) selected
                                        @endif>USN</option>
                                <option value="UYI" @if(config('settings.currencyId')=="UYI" ) selected
                                        @endif>UYI</option>
                                <option value="UYU" @if(config('settings.currencyId')=="UYU" ) selected
                                        @endif>UYU</option>
                                <option value="UZS" @if(config('settings.currencyId')=="UZS" ) selected
                                        @endif>UZS</option>
                                <option value="VEF" @if(config('settings.currencyId')=="VEF" ) selected
                                        @endif>VEF</option>
                                <option value="VND" @if(config('settings.currencyId')=="VND" ) selected
                                        @endif>VND</option>
                                <option value="VUV" @if(config('settings.currencyId')=="VUV" ) selected
                                        @endif>VUV</option>
                                <option value="WST" @if(config('settings.currencyId')=="WST" ) selected
                                        @endif>WST</option>
                                <option value="XAF" @if(config('settings.currencyId')=="XAF" ) selected
                                        @endif>XAF</option>
                                <option value="XCD" @if(config('settings.currencyId')=="XCD" ) selected
                                        @endif>XCD</option>
                                <option value="XDR" @if(config('settings.currencyId')=="XDR" ) selected
                                        @endif>XDR</option>
                                <option value="XOF" @if(config('settings.currencyId')=="XOF" ) selected
                                        @endif>XOF</option>
                                <option value="XPF" @if(config('settings.currencyId')=="XPF" ) selected
                                        @endif>XPF</option>
                                <option value="XSU" @if(config('settings.currencyId')=="XSU" ) selected
                                        @endif>XSU</option>
                                <option value="XUA" @if(config('settings.currencyId')=="XUA" ) selected
                                        @endif>XUA</option>
                                <option value="YER" @if(config('settings.currencyId')=="YER" ) selected
                                        @endif>YER</option>
                                <option value="ZAR" @if(config('settings.currencyId')=="ZAR" ) selected
                                        @endif>ZAR</option>
                                <option value="ZMW" @if(config('settings.currencyId')=="ZMW" ) selected
                                        @endif>ZMW</option>
                                <option value="ZWL" @if(config('settings.currencyId')=="ZWL" ) selected
                                        @endif>ZWL</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Delivery Type:</label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" name="delivery_type" required>
                                <option value="1" class="text-capitalize">Delivery</option>
                                <option value="2" class="text-capitalize">Self Pickup</option>
                                <option value="3" class="text-capitalize">Both Delivery & Self Pickup</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>{{__('storeDashboard.seLblFullAddress')}}:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg" name="address"
                                placeholder="{{__('storeDashboard.sePhFullAddress')}}" required>
                        </div>
                    </div>
                    {{--<div class="form-group row">--}}
                        {{--<label class="col-lg-3 col-form-label" data-popup="tooltip" title="{{__('storeDashboard.seToolTipPincode')}}" data-placement="bottom">{{__('storeDashboard.seLblPincode')}}:</label>--}}
                        {{--<div class="col-lg-9">--}}
                            {{--<input type="text" class="form-control form-control-lg" name="pincode"--}}
                                {{--placeholder="{{__('storeDashboard.seToolTipPincode')}}">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="form-group row">--}}
                        {{--<label class="col-lg-3 col-form-label">{{__('storeDashboard.seLblLandMark')}}:</label>--}}
                        {{--<div class="col-lg-9">--}}
                            {{--<input type="text" class="form-control form-control-lg" name="landmark"--}}
                                {{--placeholder="{{__('storeDashboard.sePhLandMark')}}">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <fieldset class="gllpLatlonPicker">
                        {{-- <div width="100%" id="map" class="gllpMap" style="position: relative; overflow: hidden;"></div> --}}
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label class="col-form-label">{{__('storeDashboard.seLblLat')}}:</label><input type="text" class="form-control form-control-lg gllpLatitude latitude" value="40.6976701" name="latitude" placeholder="Latitude of the Restaurant" required="required">
                            </div>
                            <div class="col-lg-6">
                                <label class="col-form-label">{{__('storeDashboard.seLblLong')}}:</label><input type="text" class="form-control form-control-lg gllpLongitude longitude" value="-74.2598672" name="longitude" placeholder="Longitude of the Restaurant" required="required">
                            </div>
                        </div>
                       {{--  <input type="hidden" class="gllpZoom" value="20">
                        <div class="d-flex justify-content-center">
                            <div class="col-lg-6 d-flex location-search-block">       
                                <input type="text" class="form-control form-control-lg gllpSearchField" placeholder="Search for resraurant, city or town...">
                                <button type="button" class="btn btn-primary gllpSearchButton">{{__('storeDashboard.seLblSearch')}}</button>
                            </div>
                        </div> --}}
                        <span class="text-muted">{{__('storeDashboard.sePhTextLatLong1')}} <a href="https://www.mapcoordinates.net/en" target="_blank">https://www.mapcoordinates.net/en</a></span> <br> {{__('storeDashboard.sePhTextLatLong2')}}
                    </fieldset>
                    <hr>
                    {{--<div class="form-group row">--}}
                        {{--<label class="col-lg-3 col-form-label">{{__('storeDashboard.seLblCertificate')}}:</label>--}}
                        {{--<div class="col-lg-9">--}}
                            {{--<input type="text" class="form-control form-control-lg" name="certificate"--}}
                                {{--placeholder="{{__('storeDashboard.sePhCertificate')}}">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">{{__('storeDashboard.seLblStoreCharge')}}:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg restaurant_charges" name="restaurant_charges"
                                placeholder="{{__('storeDashboard.sePhStoreCharge')}} {{ config('settings.currencyFormat') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">{{__('storeDashboard.seLblPureVeg')}}</label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switchery mt-2">
                                <label>
                                <input value="true" type="checkbox" class="switchery-primary" checked="checked" name="is_pureveg">
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">{{__('storeDashboard.seLblMinOrderPrice')}}:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg min_order_price" name="min_order_price"
                                placeholder="{{__('storeDashboard.sePhMinOrderPrice')}} {{ config('settings.currencyFormat') }}" value="0">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Tax
                            :</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg commission_rate"
                                   name="tax"  placeholder="Tax" required>
                        </div>
                    </div>
                    @csrf
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">
                        {{__('storeDashboard.save')}}
                        <i class="icon-database-insert ml-1"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function readURL2(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function (e) {
                $('.slider-preview-image2')
                    .removeClass('hidden')
                    .attr('src', e.target.result)
                    .width(160)
                    .height(117)
                    .css('borderRadius', '0.275rem');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL(input) {
       if (input.files && input.files[0]) {
           let reader = new FileReader();
           reader.onload = function (e) {
               $('.slider-preview-image')
                   .removeClass('hidden')
                   .attr('src', e.target.result)
                   .width(160)
                   .height(117)
                   .css('borderRadius', '0.275rem');
           };
           reader.readAsDataURL(input.files[0]);
       }
    }
    $(function () {
       $('.select-search').select2({
           minimumResultsForSearch: Infinity,
           placeholder: 'Select Location',
       });
    
       var primary = document.querySelector('.switchery-primary');
       var switchery = new Switchery(primary, { color: '#2196F3' });
       
       $('.form-control-uniform').uniform();

       $('.delivery_time').numeric({allowThouSep:false});
       $('.price_range').numeric({allowThouSep:false});
       $('.latitude').numeric({allowThouSep:false});
       $('.longitude').numeric({allowThouSep:false});
       $('.restaurant_charges').numeric({ allowThouSep:false, maxDecimalPlaces: 2 });
       $('.delivery_charges').numeric({ allowThouSep:false, maxDecimalPlaces: 2 });
        $('.min_order_price').numeric({ allowThouSep:false, maxDecimalPlaces: 2, allowMinus: false });
    });
    
</script>
@endsection