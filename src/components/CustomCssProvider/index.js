import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { getSingleLanguageData,getAllLanguages } from "../../services/languages/actions";
class CustomCssProvider extends Component {
	componentDidMount() {
		console.log('i reached your code------->');
		
			if (localStorage.getItem("userPreferedLanguage")) {
				this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage"));
			} else {
				
					this.props.getSingleLanguageData(1);
				
			}
		
		// this.props.getSingleLanguageData(2);
		// this.props.getAllLanguages();
		// localStorage.setItem("userPreferedLanguage", 1);
	}
	render() {
		
		return (
			<React.Fragment>
				{localStorage.getItem("customCSS") !== null && (
					<Helmet>
						<style type="text/css">{localStorage.getItem("customCSS")}</style>
					</Helmet>
				)}
			</React.Fragment>
		);
	}
}

const mapStateToProps = (state) => ({
	settings: state.settings.settings,
});

export default connect(
	mapStateToProps,
	{getSingleLanguageData,getAllLanguages}
)(CustomCssProvider);
