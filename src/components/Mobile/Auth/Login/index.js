import React, { Component } from "react";
import {
  loginUser,
  registerUser,
  sendOtp,
  verifyOtp,
} from "../../../../services/user/actions";

import BackButton from "../../Elements/BackButton";
import { NavLink } from "react-router-dom";
import { Redirect } from "react-router";
import SimpleReactValidator from "simple-react-validator";
import { connect } from "react-redux";
import Loading from "../../../helpers/loading";

class Login extends Component {
  constructor() {
    super();
    this.validator = new SimpleReactValidator({
      autoForceUpdate: this,
      messages: {
        required: localStorage.getItem("fieldValidationMsg"),
        email: localStorage.getItem("emailValidationMsg"),
        regex: localStorage.getItem("phoneValidationMsg"),
      },
    });
  }

  state = {
    loading: false,
    email: "",
    name: "",
    phone: "",
    password: "",
    otp: "",
    accessToken: "",
    provider: "",
    error: false,
    email_phone_already_used: false,
    invalid_otp: false,
    showResendOtp: false,
    countdownStart: false,
    countDownSeconds: 15,
    email_pass_error: false,
  };

  static contextTypes = {
    router: () => null,
  };

  componentDidMount() {}

  handleInputChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleLogin = (event) => {
    console.log( this.state.name,   this.state.phone);
    event.preventDefault();
    if (
      this.validator.fieldValid("phone") &&
      this.validator.fieldValid("name")
    ) {
      this.setState({ loading: true });
      this.props.loginUser(
        this.state.name,
        null,
        null,
        null,
        this.state.phone,
        null,
        JSON.parse(localStorage.getItem("userSetAddress"))
      );
      // window.location.href = "/checkout";
    } else {
      console.log("validation failed");
      this.validator.showMessages();
    }

    //     event.preventDefault();
    //     if (this.state.name && this.state.phone) {
    //       this.setState({ loading: true });
    //       this.props.loginUser(
    //         this.state.name,
    //         this.state.phone,
    // 		JSON.parse(localStorage.getItem("userSetAddress"))
    // 	  );
    // 	  window.location.href = "/checkout";
    //     } else {
    //       console.log("validation failed");
    //       this.validator.showMessages();
    //     }
  };

  resendOtp = () => {
    if (this.validator.fieldValid("phone")) {
      this.setState({ countDownSeconds: 15, showResendOtp: false });
      this.props
        .sendOtp(this.state.email, this.state.phone, null)
        .then((response) => {
          if (!response.payload.otp) {
            this.setState({ error: false });
          }
        });
    }
  };

  handleVerifyOtp = (event) => {
    event.preventDefault();
    console.log("verify otp clicked");
    if (this.validator.fieldValid("otp")) {
      this.setState({ loading: true });
      this.props.verifyOtp(this.state.phone, this.state.otp);
    }
  };

  handleOnChange = (event) => {
    this.props.getSingleLanguageData(event.target.value);
    localStorage.setItem("userPreferedLanguage", event.target.value);
  };
  componentWillReceiveProps(nextProps) {
    const { user } = this.props;
    if (user !== nextProps.user) {
      this.setState({ loading: false });
    }
    if (nextProps.user.success) {
      if (nextProps.user.data.default_address !== null) {
        const userSetAddress = {
          lat: nextProps.user.data.default_address.latitude,
          lng: nextProps.user.data.default_address.longitude,
          address: nextProps.user.data.default_address.address,
          house: nextProps.user.data.default_address.house,
          tag: nextProps.user.data.default_address.tag,
        };
        localStorage.setItem("userSetAddress", JSON.stringify(userSetAddress));
      }
      this.context.router.history.goBack();
    }
    if (nextProps.user.email_phone_already_used) {
      this.setState({ email_phone_already_used: true });
    }
    if (nextProps.user.otp) {
      this.setState({ email_phone_already_used: false, error: false });
      //otp sent, hide reg form and show otp form
      document.getElementById("loginForm").classList.add("hidden");
      document.getElementById("socialLoginDiv").classList.add("hidden");
      document
        .getElementById("phoneFormAfterSocialLogin")
        .classList.add("hidden");
      document.getElementById("otpForm").classList.remove("hidden");

      //start countdown
      this.setState({ countdownStart: true });
      this.handleCountDown();
      this.validator.hideMessages();
    }

    if (nextProps.user.valid_otp) {
      this.setState({ invalid_otp: false, error: false, loading: true });
      // register user
      if (this.state.social_login) {
        this.props.loginUser(
          this.state.name,
          this.state.email,
          null,
          this.state.accessToken,
          this.state.phone,
          this.state.provider,
          JSON.parse(localStorage.getItem("userSetAddress"))
        );
      } else {
        this.props.registerUser(
          this.state.name,
          this.state.email,
          this.state.phone,
          this.state.password,
          JSON.parse(localStorage.getItem("userSetAddress"))
        );
      }

      console.log("VALID OTP, REG USER NOW");
      // this.setState({ loading: false });
    }

    if (nextProps.user.valid_otp === false) {
      console.log("Invalid OTP");
      this.setState({ invalid_otp: true });
    }

    if (!nextProps.user) {
      this.setState({ error: true });
    }

    //old user, proceed to login after social login
    if (nextProps.user.proceed_login) {
      this.setState({ loading: true });
      console.log("From Social : user already exists");
      this.props.loginUser(
        this.state.name,
        this.state.email,
        null,
        this.state.accessToken,
        null,
        this.state.provider,
        JSON.parse(localStorage.getItem("userSetAddress"))
      );
    }

    if (nextProps.user.enter_phone_after_social_login) {
      this.validator.hideMessages();
      document.getElementById("loginForm").classList.add("hidden");
      document.getElementById("socialLoginDiv").classList.add("hidden");
      document
        .getElementById("phoneFormAfterSocialLogin")
        .classList.remove("hidden");
      // populate name & email
      console.log("ask to fill the phone number and send otp process...");
    }

    if (nextProps.user.data === "DONOTMATCH") {
      //email and pass donot match
      this.setState({ error: false, email_pass_error: true });
    }

    if (this.props.languages !== nextProps.languages) {
      if (localStorage.getItem("userPreferedLanguage")) {
        this.props.getSingleLanguageData(
          localStorage.getItem("userPreferedLanguage")
        );
      } else {
        if (nextProps.languages.length) {
          console.log("Fetching Translation Data...");
          const id = nextProps.languages.filter(
            (lang) => lang.is_default === 1
          )[0].id;
          this.props.getSingleLanguageData(id);
        }
      }
    }
  }

  handleCountDown = () => {
    setTimeout(() => {
      this.setState({ showResendOtp: true });
      clearInterval(this.intervalID);
    }, 15000 + 1000);
    this.intervalID = setInterval(() => {
      console.log("interval going on");
      this.setState({
        countDownSeconds: this.state.countDownSeconds - 1,
      });
    }, 1000);
  };

  componentWillUnmount() {
    //clear countdown
    console.log("Countdown cleared");
    clearInterval(this.intervalID);
  }

  render() {
    // console.log("Langugaes", this.props.languages);

    if (window.innerWidth > 768) {
      return <Redirect to="/" />;
    }
    if (localStorage.getItem("storeColor") === null) {
      return <Redirect to={"/"} />;
    }
    const { user } = this.props;
    if (user.success) {
      return (
        //redirect to account page
        <Redirect to={"/my-account"} />
      );
    }
    // const languages = JSON.parse(localStorage.getItem("state")).languages;
    // const languages = this.props.language;
    // console.log(languages);

    const languages = this.props.languages;

    return (
      <React.Fragment>
        {this.state.error && (
          <div className="auth-error">
            <div className="error-shake">
              {localStorage.getItem("loginErrorMessage")}
            </div>
          </div>
        )}
        {this.state.email_phone_already_used && (
          <div className="auth-error">
            <div className="error-shake">
              {localStorage.getItem("emailPhoneAlreadyRegistered")}
            </div>
          </div>
        )}
        {this.state.invalid_otp && (
          <div className="auth-error">
            <div className="error-shake">
              {localStorage.getItem("invalidOtpMsg")}
            </div>
          </div>
        )}
        {this.state.email_pass_error && (
          <div className="auth-error">
            <div className="error-shake">
              {localStorage.getItem("emailPassDonotMatch")}
            </div>
          </div>
        )}

        {this.state.loading && <Loading />}

        <div style={{ backgroundColor: "#f2f4f9" }}>
          <div className="input-group">
            <div className="input-group-prepend">
              <BackButton history={this.props.history} />
            </div>
          </div>
          <img
            src="/assets/img/login-header.png"
            className="login-image pull-right mr-15"
            alt="login-header"
          />
          <div className="login-texts px-15 mt-50 pb-20">
            <span className="login-title">
              Confirming Order
              {/* {localStorage.getItem("loginLoginTitle")} */}
            </span>
            <br />
            {/* <span className="login-subtitle">
              {localStorage.getItem("loginLoginSubTitle")}
            </span> */}
          </div>
        </div>
        <div className="bg-white">
          {/* <form onSubmit={this.handleLogin} id="loginForm">
						<div className="form-group px-15 pt-30">
							<label className="col-12 edit-address-input-label">
								{localStorage.getItem("loginLoginEmailLabel")}{" "}
								{this.validator.message("phone", this.state.phone, "required")}
							</label>
							<div className="col-md-9 pb-5">
								<input
									type="text"
									name="phone"
									onChange={this.handleInputChange}
									className="form-control edit-address-input"
								/>
							</div>
						</div>
						<div className="mt-20 px-15 pt-15 button-block">
							<button
								type="submit"
								className="btn btn-main"
								style={{
									backgroundColor: localStorage.getItem("storeColor"),
								}}
							>
								{localStorage.getItem("loginLoginTitle")}
							</button>
            </div>
					</form> */}

          <form onSubmit={this.handleLogin} id="loginForm">
            <div className="form-group px-15 pt-30">
              <label className="col-12 edit-address-input-label">
                {localStorage.getItem("loginLoginNameLabel")}{" "}
                {this.validator.message("name", this.state.name, "required")}
              </label>
              <div className="col-md-9 pb-5">
                <input
                  type="text"
                  name="name"
                  onChange={this.handleInputChange}
                  className="form-control edit-address-input"
                />
              </div>

              <label className="col-12 edit-address-input-label">
                {localStorage.getItem("loginLoginPhoneLabel")}{" "}
                {this.validator.message("phone", this.state.phone, "required")}
              </label>
              <div className="col-md-9 pb-5">
                <input
                  type="text"
                  name="phone"
                  onChange={this.handleInputChange}
                  className="form-control edit-address-input"
                />
              </div>
              {/*<label className="col-12 edit-address-input-label">*/}
              {/*{localStorage.getItem("loginLoginPasswordLabel")}{" "}*/}
              {/*{this.validator.message("password", this.state.password, "required")}*/}
              {/*</label>*/}
              {/*<div className="col-md-9">*/}
              {/*<input*/}
              {/*type="password"*/}
              {/*name="password"*/}
              {/*onChange={this.handleInputChange}*/}
              {/*className="form-control edit-address-input"*/}
              {/*/>*/}
              {/*</div>*/}
            </div>

            {/* <div className="mt-20 px-15 pt-15 button-block">
 							<button
 								type="submit"
 								className="btn btn-main"
 								style={{
 									backgroundColor: localStorage.getItem("storeColor"),
 								}}
 							>
 								{localStorage.getItem("loginLoginTitle")}
 							</button>
 						</div> */}

            <button
              className="btn btn-main"
              type="submit"
              style={{ backgroundColor: "green" }}
            >
              CONFIRM ORDER
            </button>
          </form>

          <form onSubmit={this.handleVerifyOtp} id="otpForm" className="hidden">
            <div className="form-group px-15 pt-30">
              <label className="col-12 edit-address-input-label">
                {localStorage.getItem("otpSentMsg")} {this.state.phone}
                {this.validator.message(
                  "otp",
                  this.state.otp,
                  "required|numeric|min:4|max:6"
                )}
              </label>
              <div className="col-md-9">
                <input
                  name="otp"
                  type="tel"
                  onChange={this.handleInputChange}
                  className="form-control edit-address-input"
                  required
                />
              </div>

              <button
                type="submit"
                className="btn btn-main"
                style={{
                  backgroundColor: localStorage.getItem("storeColor"),
                }}
              >
                {localStorage.getItem("verifyOtpBtnText")}
              </button>

              <div className="mt-30 mb-10">
                {this.state.showResendOtp && (
                  <div className="resend-otp" onClick={this.resendOtp}>
                    {localStorage.getItem("resendOtpMsg")} {this.state.phone}
                  </div>
                )}

                {this.state.countDownSeconds > 0 && (
                  <div className="resend-otp countdown">
                    {localStorage.getItem("resendOtpCountdownMsg")}{" "}
                    {this.state.countDownSeconds}
                  </div>
                )}
              </div>
            </div>
          </form>
        </div>
        {localStorage.getItem("enPassResetEmail") === "true" && (
          <div className="mt-4 d-flex align-items-center justify-content-center mb-100">
            <NavLink
              to="/login/forgot-password"
              style={{
                color: localStorage.getItem("storeColor"),
              }}
            >
              {localStorage.getItem("forgotPasswordLinkText")}
            </NavLink>
          </div>
        )}

        {localStorage.getItem("registrationPolicyMessage") !== "null" && (
          <div
            className="mt-20 mb-20 d-flex align-items-center justify-content-center"
            dangerouslySetInnerHTML={{
              __html: localStorage.getItem("registrationPolicyMessage"),
            }}
          />
        )}

        {languages && languages.length > 1 && (
          <div className="mt-4 d-flex align-items-center justify-content-center mb-100">
            <div className="mr-2">
              {localStorage.getItem("changeLanguageText")}
            </div>
            <select
              onChange={this.handleOnChange}
              defaultValue={
                localStorage.getItem("userPreferedLanguage")
                  ? localStorage.getItem("userPreferedLanguage")
                  : languages.filter((lang) => lang.is_default === 1)[0].id
              }
              className="form-control language-select"
            >
              {languages.map((language) => (
                <option value={language.id} key={language.id}>
                  {language.language_name}
                </option>
              ))}
            </select>
          </div>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.user,
  language: state.languages.language,
  languages: state.languages.languages,
 
});

export default connect(
  mapStateToProps,
  {
    loginUser,
    registerUser,
    sendOtp,
    verifyOtp,
    // getSingleLanguageData,
  }
)(Login);
