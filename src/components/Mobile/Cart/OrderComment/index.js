import React, { Component } from "react";

class OrderComment extends Component {
    state = {
        comment: ""
    };

    componentDidMount() {
        this.setState({ comment: localStorage.getItem("orderComment") });
    }

    handleInput = event => {
        this.setState({ comment: event.target.value });
        localStorage.setItem("orderComment", event.target.value);
    };

    render() {
        return (
            <React.Fragment>
                <div id='commentid'>
                    <div className='form-control1  mb-1'>
                        <h2 className='item-text mb-10'>{localStorage.getItem("cartSuggestionPlaceholder")}</h2>
                    </div>
                    {/* <div className='line'>

                    </div> */}
                <input
                    
                    className="form-control order-comment mb-20 bb1
                    "
                    type="text"
                    placeholder={localStorage.getItem("cartSuggestionPlaceholder")}
                    onChange={this.handleInput}
                    value={this.state.comment || ""}
                />
                </div>
                
            </React.Fragment>
        );
    }
}

export default OrderComment;
