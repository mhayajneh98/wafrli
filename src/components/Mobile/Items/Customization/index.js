import React, { Component } from "react";

import Ink from "react-ink";

import Modal from "react-responsive-modal";
import Fade from "react-reveal/Fade";

class Customization extends Component {
	state = {
		open: false,
		size: 0,
		notInRange: false,
		continueClicked: false,
		required: {},
		alert: {},
	};

	_processAddons = (product) => {
		let addons = [];
		addons["selectedaddons"] = [];
		this.setState({
			continueClicked: true
		});
		let empty = {};
		this.setState({
			alert: empty,
		});


		let radio = document.querySelectorAll("input[type=radio]:checked");
		for (let i = 0; i < radio.length; i++) {
			addons["selectedaddons"].push({
				addon_category_name: radio[i].name,
				addon_id: radio[i].getAttribute("data-addon-id"),
				addon_name: radio[i].getAttribute("data-addon-name"),
				price: radio[i].value,
			});
		}

		let checkboxes = document.querySelectorAll("input[type=checkbox]:checked");

		for (let i = 0; i < checkboxes.length; i++) {
			addons["selectedaddons"].push({
				addon_category_name: checkboxes[i].name,
				addon_id: checkboxes[i].getAttribute("data-addon-id"),
				addon_name: checkboxes[i].getAttribute("data-addon-name"),
				price: checkboxes[i].value,
			});
		}
		console.log('after continue-------->', this.state)
		if (this.checkMinMax(addons)) {
			this.props.addProduct(Object.assign(addons, product));
			this.handlePopupClose();
		} else {
			this.setState({ notInRange: true });

			setTimeout(() => {
				this.setState({ notInRange: false });
			}, 4 * 1000);
		}

	};
	checkMinMax = (addonsSelected) => {
		let require = {};

		if (this.props.product.sizes.length) {
			console.log('1------------------>', !!this.props.product.sizes[this.state.size])
			if (this.props.product.sizes[this.state.size]) {
				console.log('2------------------>', !!this.props.product.sizes[this.state.size].addon_categories.length)
				if (this.props.product.sizes[this.state.size].addon_categories.length) {
					this.props.product.sizes[this.state.size].addon_categories.forEach(add => {
						console.log('add------------------>', add, add.is_required)
						if (add.is_required) {
							console.log('add min max------------------>', add.min, add.max)
							//  this.setState({required : {...this.state.required,[add.name]:{
							// 	 min:add.min,
							// 	max:add.max,
							//  }
							// }})
							require[add.name]={
									 min:add.min,
									max:add.max,
								 }

						}
					})


				}
			}
		}

		this.props.product.addon_categories.forEach(categories => {
			if (categories.is_required) {
				console.log('add min max------------------>', categories.min, categories.max)

				// this.setState({
				// 	required: Object.assign(this.state.required, {
				// 		[categories.name]: {
				// 			min: categories.min,
				// 			max: categories.max,
				// 		}
				// 	})
				// });
				require[categories.name]={
					min:categories.min,
				   max:categories.max,
				}

			}

		})
		console.log('check min max--->', require);
		let requireItems = Object.keys(require);
		console.log('requireItems--->', requireItems);
		for (let index = 0; index < requireItems.length; index++) {
			console.log('selectedLength---------->', addonsSelected["selectedaddons"].filter(selected => selected.addon_category_name == requireItems[index]).length, addonsSelected["selectedaddons"].filter(selected => selected.addon_category_name == requireItems[index]))
			let selectedLength = addonsSelected["selectedaddons"].filter(selected => selected.addon_category_name == requireItems[index]).length;
			if (selectedLength < require[requireItems[index]].min || selectedLength >require[requireItems[index]].max) {
				this.setState({
					alert: Object.assign(this.state.alert, {
						[requireItems[index]]: true
					})


				})
				// this.setState({
				// 	alert: Object.assign(this.state.alert, {
				// 		[requireItems[index]]: true
				// 	})


				// })
				console.log('required------------------->', requireItems[index])

			}

		}
		// if (Object.keys(this.state.alert).length) {
		// 	return false;
		// }
	

		for (const [key, value] of Object.entries(this.state.alert)) {
			console.log('key----->',key, value)
			if (value) {
				console.log('true--->',key, value)
				return false;
			}
		}
		return true;
	}
	handlePopupOpen = () => {
		this.setState({ open: true });
	};
	handlePopupClose = () => {
		this.setState({ alert: {}, required: {} })
		this.setState({ open: false });
		this.setState({ size: 0 })
		this.props.forceUpdate();


	};

	render() {
		const { product } = this.props;
		return (
			<React.Fragment>
				<button
					type="button"
					className="btn btn-add-remove"
					style={{
						color: localStorage.getItem("cartColor-bg"),
					}}
					onClick={this.handlePopupOpen}
				>
					<span className="btn-inc">+</span>
					<Ink duration="500" />
				</button>
				<Modal open={this.state.open} onClose={this.handlePopupClose} closeIconSize={32}>
					<div
						style={{
							marginTop: "5rem",
							textAlign: "left",
						}}
					>
						<h3>{localStorage.getItem("customizationHeading")}</h3>

						{console.log(product.name, product.sizes.length)}
						{product.sizes.length ? (
							<>
								<p className="addon-category-name">{'size'}</p>
								<fieldset>
									{product.sizes.map((size, index) => (
										<React.Fragment key={size.id}>
											<div className="form-group addon-list" 
											onClick={()=>{
												this.setState(prevState => {
													let alert = { ...prevState.alert };  // creating copy of state variable jasper
													for (const property in alert) {
														alert[property] = false;
													}
													console.log('alert------>',alert)
													return { alert };                                 // return new object jasper object
												})
												
										}}>
												<input
													type=
													'radio'

													className=
													'magic-radio'

													name={'size.name'}
													data-addon-id={size.id}
													data-addon-name={size.name}
													value={size.price}
													defaultChecked={
														index === 0 && true
													}
													onClick={() => this.setState({ size: index })}
												/>
												{
													<label htmlFor={size.name} />
												}

												<label className="text addon-label" htmlFor={size.name}>
													{size.name}{"      "}
													{/* localStorage.getItem("hidePriceWhenZero") === "true" && */}
													{
														size.price === "0.00" ? null : (
															<React.Fragment>
																{/* {localStorage.getItem("currencySymbolAlign") ===
																	"left" &&
																	localStorage.getItem("currencyFormat")} */}
																<span className='ml-10 bold-400'>
																	+{size.price}{" "}
																</span>

																{/* {localStorage.getItem("currencySymbolAlign") ===
																	"right" &&
																	localStorage.getItem("currencyFormat")} */}
															</React.Fragment>
														)}
												</label>
											</div>

										</React.Fragment>

									))}
									<hr className='customize-line' />
								</fieldset>
							</>
						) : null}



						{/* ...product.sizes[this.state.size].addon_categories, */}
						{console.log('addon category---->', !!product.sizes.length, [(product.sizes.length ? product.sizes[this.state.size].addon_categories : [])], product.sizes[this.state.size])}
						{[...(product.sizes.length ? product.sizes[this.state.size].addon_categories : []), ...product.addon_categories].map((addon_category) => (
							<div key={addon_category.id} className="addon-category-block mt-8">

								<React.Fragment>

									<p className="addon-category-name">{addon_category.name}</p>
									{addon_category.addons.length && (
										<React.Fragment>
											{addon_category.is_required && this.state.alert[addon_category.name] ?
												<p style={{ color: 'red' }}
												>this field should be between {addon_category.min} and {addon_category.max}</p> :
												null}
											{addon_category.addons.map((addon, index) => (
												<React.Fragment key={addon.id}>
													<div className="form-group addon-list"
														onClick={() => {
															this.setState(prevState => {
																let alert = { ...prevState.alert };  // creating copy of state variable jasper
																alert[addon_category.name] = false;                     // update the name property, assign a new value                 
																return { alert };                                 // return new object jasper object
															});
															this.setState({ required: {} });
														}
														}>

														<input
															type={
																addon_category.type === "SINGLE" ? "radio" : "checkbox"
															}
															className={
																addon_category.type === "SINGLE"
																	? "magic-radio"
																	: "magic-checkbox"
															}
															name={addon_category.name}
															data-addon-id={addon.id}
															data-addon-name={addon.name}
															value={addon.price}
															defaultChecked={
																addon_category.type === "SINGLE" && index === 0 && true
															}
														/>
														{addon_category.type === "SINGLE" && (
															<label htmlFor={addon.name} />
														)}

														<label className="text addon-label" htmlFor={addon.name}>
															{addon.name}{" "}
															{/* localStorage.getItem("hidePriceWhenZero") === "true" && */}
															{
																addon.price === "0.00" ? null : (
																	<React.Fragment>
																		{/* {localStorage.getItem("currencySymbolAlign") ===
																			"left" &&
																			localStorage.getItem("currencyFormat")} */}
																		<span className='ml-10 bold-400'>
																			+{addon.price}{" "}
																		</span>
																		{/* {localStorage.getItem("currencySymbolAlign") ===
																			"right" &&
																			localStorage.getItem("currencyFormat")} */}
																	</React.Fragment>
																)}
														</label>
													</div>
												</React.Fragment>
											))}
										</React.Fragment>
									)}
									<hr className='customize-line' />
								</React.Fragment>
							</div>
						))}

						<button
							className="btn btn-lg btn-customization-done"
							onClick={() => {
								this._processAddons(product);
								// this.handlePopupClose();
							}}
							style={{
								backgroundColor: localStorage.getItem("cartColorBg"),
								color: localStorage.getItem("cartColorText"),
							}}
						>
							{localStorage.getItem("customizationDoneBtnText")}
						</button>
					</div>
					{this.state.notInRange && (
						<Fade duration={250} bottom>
							<div className="auth-error going-different-restaurant-notify check-range">
								<div className="">{localStorage.getItem("itemsRemovedMsg")}</div>
							</div>
						</Fade>
					)}
				</Modal>

			</React.Fragment>
		);
	}
}

export default Customization;
