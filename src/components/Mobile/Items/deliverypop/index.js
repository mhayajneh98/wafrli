import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { selectMethod } from "../../../../services/cart/actions";

import Collapsible from "react-collapsible";
const PopUp = (props) => {


    return (
        <>
            {props.pop ? (
                <>

                    <div
                        className='shadow'
                    >
                        <div className='popupback'
                            onClick={() => { props.close() }}
                        >
                        </div>
                        <div className='content1'>
                            <div className='popheader'>
                                <p className='blodp'>{localStorage.getItem("searchPopularPlaces")}</p>
                                <div className='line'>
                                </div>
                            </div>
                            <div className="list1">
                                {/* Object.keys(props.city) */}
                                {props.city.map(city => {
                                    return (
                                        (<Collapsible key={city.name_en} trigger={city.name_en}>
                                            {/* Object.keys(props.city[city].area) */}
                                            {/* [{name_en:'abu nsair'},{name_en:'hunaiha'},{name_en:'khalda'}] */}
                                            {city.delivery_areas.map(area => {
                                                console.log('poparea-->', area)
                                                return (
                                                    <>
                                                        <div className='listitem'
                                                            onClick={() => {
                                                                props.selectMethod({ ...area, method: 'Delivery' })
                                                                props.select(area.area_name)
                                                            }}>
                                                            <p>{area.area_name}</p>

                                                        </div>
                                                        <div className='line'></div>
                                                    </>
                                                )
                                            })
                                            }
                                        </Collapsible>)

                                    )
                                })}
                            </div>

                        </div>


                    </div>
                </>) : null}
        </>

    )
}
const mapStateToProps = (state) => ({

});
export default connect(
    mapStateToProps,
    { selectMethod }
)(PopUp);
