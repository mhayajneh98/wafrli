import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { selectMethod } from "../../../../services/cart/actions";

const PopUp = (props) => {


    return (
        <>
            {props.pop ? <div
                className='shadow'
                onClick={() => { props.close() }}>
                <div className='content1'>
                    <div className='popheader'>
                        <p className='blodp'>
                        {localStorage.getItem("runningOrderReadyForPickup")}</p>
                        <div className='line'>
                        </div>
                    </div>
                    <div className="list1 Collapsible__contentInner">
                        {props.branches.map(branch => {
                            return (
                                <>
                                    <div key={branch.name} onClick={() => { 
                                        props.selectMethod({...branch,method:'PickUp'})
                                        props.select(branch.name) }} 
                                    className='listitem'>
                                        <p >
                                            {branch.name}
                                        </p>
                                    </div>
                                    <div className='line'></div>
                                </>

                            )
                        }
                        )
                        }
                    </div>

                </div>


            </div> : null}
        </>

    )
}

const mapStateToProps = (state) => ({
  
});
export default connect(
    mapStateToProps,
    { selectMethod }
)(PopUp);