import { LOAD_CART, ADD_PRODUCT, REMOVE_PRODUCT,SELECT_METHOD } from "./actionTypes";

const initialState = {
    products: [],
    method:{method:'',},
};

export default function(state = initialState, action) {
    switch (action.type) {
        case LOAD_CART:
            return {
                ...state,
                products: action.payload
            };
        case ADD_PRODUCT:
            return {
                ...state,
                productToAdd: Object.assign({}, action.payload)
            };
        case REMOVE_PRODUCT:
            return {
                ...state,
                productToRemove: Object.assign({}, action.payload)
            };
            case SELECT_METHOD:
            return {
                ...state,
                method:action.payload,
            };
        default:
            return state;
    }
}
