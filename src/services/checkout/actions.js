import { PLACE_ORDER } from "./actionTypes";
import { PLACE_ORDER_URL } from "../../configs";
import { APPLY_COUPON } from "../coupon/actionTypes";

import Axios from "axios";
import { updateCart } from "../total/actions";
// deliveryAreaRestaurantID
// Method

// تكون فيها الmethod طريقة الدفع
// delivery_type done
// Delivery type ١ اذا كان delivery

// و 2 اذا كان pickup
// branch_id
// size_id
export const placeOrder = (
	user,
	order,
	coupon,
	location,
	order_comment,
	total,
	method,
	payment_token,
	delivery_type,
	partial_wallet,
	distance,
	orderMethod
) => (dispatch, getState) => {
console.log('orderMethod----->',orderMethod,order)
let body = {
	token: user.data.auth_token,
	user: user,
	// order: order,
	coupon: coupon,
	location: location,
	order_comment: order_comment,
	total: total,
	method: method,
	payment_token: payment_token,
	// delivery_type: delivery_type,
	delivery_type:orderMethod.method==='PickUp'?2:1,
	partial_wallet: partial_wallet,
	dis: distance,
}
if(orderMethod.method==='PickUp'){
body['branch_id']=orderMethod.id;
}else{
	body['deliveryAreaRestaurantID']=orderMethod.id;
}
order=order.map(oneOrder => {
	if(oneOrder.selectedaddons){
		
		// for (let index = 0; index < oneOrder.selectedaddons.length; index++) {
		// 	if(oneOrder.selectedaddons[index].addon_category_name==="size.name"){
		// 		size_id=index;
				
				
		// 		break;
		// 	}
			
		// }
		if(oneOrder.selectedaddons[0].addon_category_name==="size.name"){
			console.log('changed')
			oneOrder.size_id=Number(oneOrder.selectedaddons[0].addon_id);
			oneOrder.selectedaddons.splice(0,1);
		}
		
	}else{
		oneOrder.size_id=0;
	}
	console.log('after change',oneOrder);
	return oneOrder
	
});
	return Axios.post(PLACE_ORDER_URL,{...body,order} )
		.then((response) => {
			const checkout = response.data;

			if (checkout.success) {
				dispatch({ type: PLACE_ORDER, payload: checkout });

				const state = getState();
				// console.log(state);
				const cartProducts = state.cart.products;
				// const user = state.user.user;
				localStorage.removeItem("orderComment");

				for (let i = cartProducts.length - 1; i >= 0; i--) {
					// remove all items from cart
					cartProducts.splice(i, 1);
				}

				dispatch(updateCart(cartProducts));

				localStorage.removeItem("appliedCoupon");
				const coupon = [];
				dispatch({ type: APPLY_COUPON, payload: coupon });
			} else {
				return checkout;
			}
		})
		.catch(function(error) {
			return error.response;
		});
};
