<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    public function addon_categories()
    {
        return $this->belongsToMany(AddonCategory::class);
    }
    public function item()
    {
        return $this->belongsTo(Item::class);
    }
    /**
     * @return mixed
     */
    public function toggleActive()
    {
        $this->is_active = !$this->is_active;
        return $this;
    }
}
