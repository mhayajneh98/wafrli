<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function deliveryAreas()
    {
        return $this->hasMany('App\DeliveryArea');
    }
    public function scopeWithAndWhereHas($query, $relation, $constraint){
        return $query->whereHas($relation, $constraint)
            ->with([$relation => $constraint]);
    }
}
