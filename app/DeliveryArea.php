<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryArea extends Model
{
    protected $table = 'delivery_area';
    protected $fillable = ['zone_type', 'delivery_area_lat_lon', 'area_name','area_name_ar', 'city_id'];
    protected $appends = ['price', 'delivery_restaurant'];

    public function getPriceAttribute()
    {
        if ( DeliveryAreaRestaurant::where('delivery_area_id', $this->id)->first() != null) {
            return $this->attributes['price'] = DeliveryAreaRestaurant::where('delivery_area_id', $this->id)->first()->price;
        }
    }
    public function getDeliveryRestaurantAttribute()
    {
        if ( DeliveryAreaRestaurant::where('delivery_area_id', $this->id)->first() != null) {
            return $this->attributes['price'] = DeliveryAreaRestaurant::where('delivery_area_id', $this->id)->first()->id;
        }
    }

}
