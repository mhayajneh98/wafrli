<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryAreaRestaurant extends Model
{
    protected $table = 'delivery_area_restaurant';
    /**
     * @return mixed
     */
    public function toggleActive()
    {
        $this->is_active = !$this->is_active;
        return $this;
    }
}
